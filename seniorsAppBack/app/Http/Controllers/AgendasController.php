<?php

namespace App\Http\Controllers;

use App\Models\Agendas;
use Illuminate\Http\Request;

class AgendasController extends Controller
{

    public function createAgenda(Request $request,$cuenta)
    {
        $agenda = new Agendas();
        $agenda->agenda_cuenta_id = $cuenta;
        $agenda->save();
        return response()->json($agenda);


    }


    public function showOne($cuenta)
    {
        $agenda= Agendas::where('agenda_cuenta_id',$cuenta)->first();
        if(!$agenda){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe agenda en esa cuenta'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$agenda],200
        );
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Agendas  $agendas
     * @return \Illuminate\Http\Response
     */
    public function show(Agendas $agendas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Agendas  $agendas
     * @return \Illuminate\Http\Response
     */
    public function edit(Agendas $agendas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Agendas  $agendas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agendas $agendas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Agendas  $agendas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agendas $agendas)
    {
        //
    }
}
