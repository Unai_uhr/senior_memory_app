<?php

namespace App\Http\Controllers;

use App\Models\Albums_fotos;
use Illuminate\Http\Request;

class AlbumsFotosController extends Controller
{
    public function createAlbum(Request $request,$cuenta)
    {
        $album = new Albums_fotos();
        $album->album_fotos_cuenta_id = $cuenta;
        $album->save();
        return response()->json($album);


    }


    public function showOne($cuenta)
    {
        $album= Albums_fotos::where('album_fotos_cuenta_id',$cuenta)->first();
        if(!$album){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe album de fotos en esa cuenta'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$album],200
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Albums_fotos  $albums_fotos
     * @return \Illuminate\Http\Response
     */
    public function show(Albums_fotos $albums_fotos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Albums_fotos  $albums_fotos
     * @return \Illuminate\Http\Response
     */
    public function edit(Albums_fotos $albums_fotos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Albums_fotos  $albums_fotos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Albums_fotos $albums_fotos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Albums_fotos  $albums_fotos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Albums_fotos $albums_fotos)
    {
        //
    }
}
