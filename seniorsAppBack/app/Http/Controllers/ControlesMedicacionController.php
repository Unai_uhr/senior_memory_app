<?php

namespace App\Http\Controllers;

use App\Models\Controles_medicacion;
use App\Models\Medicamentos;
use App\Models\Items_control;
use Illuminate\Http\Request;

class ControlesMedicacionController extends Controller
{

    public function createControlMedicacion($horarioId,$fecha){
        
        $control = new Controles_medicacion();
        $control->controles_medicacion_horario_id = $horarioId;
        $control->controles_medicacion_fecha = $fecha;
        $control->save();
        return response()->json($control);

    }


    public function getOneControlMedicacion($horarioId,$fecha){
        
        $control= Controles_medicacion::where('controles_medicacion_horario_id',$horarioId)->where('controles_medicacion_fecha',$fecha)->first();
        if(!$control){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe control en esta cuenta'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$control],200
        );

    }

    public function getAllControlMedicacion($horarioId){

        $infoControles=array();

        $controles= Controles_medicacion::where('controles_medicacion_horario_id',$horarioId)->get();

        foreach ($controles as $control) {

            $itemsControl = Items_control::where('items_controles_control_medicaion_id',$control->id)->get();

            foreach ($itemsControl as $item) {

                $medicamento= Medicamentos::where('id',$item->items_controles_medicaion_id)->first();

                $info=array(
                    'control_id' => $control->id,
                    'horario_id' => $horarioId,
                    'fecha' => $control->controles_medicacion_fecha,
                    'medicamento_id' => $medicamento->id,
                    'medicamento_nombre' => $medicamento->medicamento_nombre,
                    'medicamento_cantidad' => $medicamento->medicamento_cantidad,
                    'medicamento_franja' => $medicamento->medicamento_franja_id,
                    'medicamento_validacion' => $item->items_controles_verificacion
                )

                array_push($infoControles,$info);

            }

            
        }

        return response() ->json(
            $infoControles
        )


        /*if(!$control){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No hay controles en esta cuenta'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$control],200
        );*/



    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Controles_medicacion  $controles_medicacion
     * @return \Illuminate\Http\Response
     */
    public function show(Controles_medicacion $controles_medicacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Controles_medicacion  $controles_medicacion
     * @return \Illuminate\Http\Response
     */
    public function edit(Controles_medicacion $controles_medicacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Controles_medicacion  $controles_medicacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Controles_medicacion $controles_medicacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Controles_medicacion  $controles_medicacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Controles_medicacion $controles_medicacion)
    {
        //
    }
}
