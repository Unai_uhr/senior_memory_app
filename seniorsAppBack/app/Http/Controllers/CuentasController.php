<?php

namespace App\Http\Controllers;

use App\Models\Cuentas;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Routing\Controller as BaseController;

class CuentasController extends BaseController
{
    public $successStatus = 200;

    public function login(){
        if(Auth::attempt(['cuenta_email' => request('cuenta_email'), 'password' => request('password')])){
            $cuenta = Auth::user();
            //$success['token'] =  $cuenta->createToken('MyApp')-> accessToken;
            return response()->json(['success' => $cuenta], $this-> successStatus);
        }
        else{
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cuenta_email' => 'required|email',
            'password' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $cuenta = User::create($input);
        //$success['token'] =  $cuenta->createToken('MyApp')-> accessToken;
        return response()->json(['success'=>$cuenta], $this-> successStatus);
    }

    public function details()
    {
        $cuenta = Auth::cuentas();
        return response()->json(['success' => $cuenta], $this-> successStatus);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createCuenta(Request $request)
    {
        $cuenta = new User();
        $cuenta->cuenta_email = $request->cuenta_email;
        $cuenta->password = $request->password;
        $cuenta->save();
        return response()->json($cuenta);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Cuentas  $cuentas
     * @return \Illuminate\Http\Response
     */
    public function showAll()
    {
        $cuentas= User::all();
        if(!$cuentas){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No hay cuentas registradas'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$cuentas],200
        );
    }

    public function showOne($email)
    {
        $cuentas= User::where('cuenta_email',$email)->first();
        if(!$cuentas){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe la cuenta con ese correo electronico'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$cuentas],200
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Cuentas  $cuentas
     * @return \Illuminate\Http\Response
     */
    public function edit(Cuentas $cuentas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Cuentas  $cuentas
     * @return \Illuminate\Http\Response
     */
    public function updateCuenta(Request $request, $id)
    {
        $cuentas= User::where('id',$id)->first();
        if(!$cuentas){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe la cuenta con ese correo electronico'
                    ])
                ],404
            );
        }else{
            $cuentaUpdated=new User();
            $cuentaUpdated->id=$id;
            $cuentaUpdated->cuenta_email=$request->cuenta_email;
            $cuentaUpdated->password=$request->password;
            $isUpdated=User::where('id',$id)->update($cuentaUpdated);
            if($isUpdated){
                return response()->json(
                    ['status'=> 'OK', 'message'=>'cuenta actualizada'],200
                );
            }else{
                return response()->json(
                    ['status'=> 'KO', 'message'=>'cuenta no actualizada'],500
                );
            }


        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Cuentas  $cuentas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Cuentas $cuentas)
    {
        //
    }
}
