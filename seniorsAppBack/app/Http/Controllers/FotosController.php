<?php

namespace App\Http\Controllers;

use App\Models\Fotos;
use Illuminate\Http\Request;

class FotosController extends Controller
{

    public function createFoto(Request $request,$albumId)
    {
        $foto = new Fotos();
        $foto->foto_album_foto_id = $albumId;
        $foto->foto_imagen=$request->foto_imagen;
        $foto->foto_comentario=$request->foto_comentario;
        $foto->save();
        return response()->json($foto);

    }

    public function showOne($fotoId)
    {
        $foto= Fotos::where('foto_album_foto_id',$fotoId)->first();
        if(!$foto){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe la foto en el album'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$foto],200
        );
    }

    public function showAll($albumId)
    {
        $fotos= Fotos::where('foto_album_foto_id',$albumId)->get();
        if(!$fotos){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No hay fotos en el album'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$fotos],200
        );
    }


    public function updateFoto(Request $request, $fotoId)
    {
        $foto= Fotos::where('id',$fotoId)->first();
        if(!$foto){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe la foto en el album'
                    ])
                ],404
            );
        }else{
            $fotoUpdated=new Fotos();
            $fotoUpdated->id=$fotoId;
            $fotoUpdated->foto_album_foto_id=$foto->foto_album_foto_id;
            $fotoUpdated->foto_imagen=$request->foto_imagen;
            $fotoUpdated->foto_comentario=$request->foto_imagen;
            $isUpdated=Fotos::where('id',$fotoUpdated->id)->update(['foto_imagen'=>$fotoUpdated->foto_imagen, 'foto_comentario'=>$fotoUpdated->foto_comentario]);
            if($isUpdated){
                return response()->json(
                    ['status'=> 'OK', 'message'=>'Foto actualizada'],200
                );
            }else{
                return response()->json(
                    ['status'=> 'KO', 'message'=>'Foto no actualizada'],500
                );
            }


        }


    }

    public function deleteFotoAlbum($foto_id){

        $fotoToDelete= Fotos::where('id',$foto_id)->delete();
        if(!$fotoToDelete){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe la foto en el álbum'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$fotoToDelete],200
        );

    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Fotos  $fotos
     * @return \Illuminate\Http\Response
     */
    public function show(Fotos $fotos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Fotos  $fotos
     * @return \Illuminate\Http\Response
     */
    public function edit(Fotos $fotos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Fotos  $fotos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Fotos $fotos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Fotos  $fotos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fotos $fotos)
    {
        //
    }
}
