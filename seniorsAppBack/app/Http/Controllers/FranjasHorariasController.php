<?php

namespace App\Http\Controllers;

use App\Models\Franjas_horarias;
use Illuminate\Http\Request;

class FranjasHorariasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Franjas_horarias  $franjas_horarias
     * @return \Illuminate\Http\Response
     */
    public function show(Franjas_horarias $franjas_horarias)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Franjas_horarias  $franjas_horarias
     * @return \Illuminate\Http\Response
     */
    public function edit(Franjas_horarias $franjas_horarias)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Franjas_horarias  $franjas_horarias
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Franjas_horarias $franjas_horarias)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Franjas_horarias  $franjas_horarias
     * @return \Illuminate\Http\Response
     */
    public function destroy(Franjas_horarias $franjas_horarias)
    {
        //
    }
}
