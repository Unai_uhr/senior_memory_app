<?php

namespace App\Http\Controllers;

use App\Models\Horarios_medicacion;
use Illuminate\Http\Request;

class HorariosMedicacionController extends Controller
{


    public function createHorario(Request $request,$cuenta)
    {
        $horario = new Horarios_medicacion();
        $horario->horario_medicacion_cuenta_id = $cuenta;
        $horario->save();
        return response()->json($horario);


    }


    public function showOne($cuenta)
    {
        $horario= Horarios_medicacion::where('horario_medicacion_cuenta_id',$cuenta)->first();
        if(!$horario){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe horario en esa cuenta'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$horario],200
        );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Horarios_medicacion  $horarios_medicacion
     * @return \Illuminate\Http\Response
     */
    public function show(Horarios_medicacion $horarios_medicacion)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Horarios_medicacion  $horarios_medicacion
     * @return \Illuminate\Http\Response
     */
    public function edit(Horarios_medicacion $horarios_medicacion)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Horarios_medicacion  $horarios_medicacion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Horarios_medicacion $horarios_medicacion)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Horarios_medicacion  $horarios_medicacion
     * @return \Illuminate\Http\Response
     */
    public function destroy(Horarios_medicacion $horarios_medicacion)
    {
        //
    }
}
