<?php

namespace App\Http\Controllers;

use App\Models\Items_agenda;
use Illuminate\Http\Request;

class ItemsAgendaController extends Controller
{

    public function createItemAgenda(Request $request,$agendaId)
    {
        $itemAgenda = new Items_agenda();
        $itemAgenda->item_agenda_agenda_id = $agendaId;
        $itemAgenda->item_agenda_fecha=$request->item_agenda_fecha;
        $itemAgenda->item_agenda_hora=$request->item_agenda_hora;
        $itemAgenda->item_agenda_item=$request->item_agenda_item;
        $itemAgenda->item_agenda_descripcion=$request->item_agenda_descripcion;
        $itemAgenda->save();
        return response()->json($itemAgenda);

    }

    public function showOne($itemId)
    {
        $itemAgenda= Items_agenda::where('id',$itemId)->first();
        if(!$itemAgenda){
            return response()->json(itemAgendaId
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe el registro en la agenda'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$itemAgenda],200
        );
    }

    public function showAll($agendaId)
    {
        $itemAgenda= Items_agenda::where('item_agenda_agenda_id',$agendaId)->get();
        if(!$itemAgenda){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No hay fotos en el album'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$itemAgenda],200
        );
    }


    public function updateItemAgenda(Request $request, $itemId)
    {
        $itemAgenda= Items_agenda::where('id',$itemId)->first();
        if(!$itemAgenda){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe el resgistro en la agenda'
                    ])
                ],404
            );
        }else{
            $itemUpdated=new Items_agenda();
            $itemUpdated->id=$itemId;
            $itemUpdated->item_agenda_agenda_id=$itemAgenda->item_agenda_agenda_id;
            $itemUpdated->item_agenda_fecha=$request->item_agenda_fecha;
            $itemUpdated->item_agenda_hora=$request->item_agenda_hora;
            $itemUpdated->item_agenda_item=$request->item_agenda_item;
            $itemUpdated->item_agenda_descripcion=$request->item_agenda_descripcion;
            $isUpdated=Items_agenda::where('id',$itemUpdated->id)->update(['item_agenda_fecha'=>$itemUpdated->item_agenda_fecha,'item_agenda_hora'=>$itemUpdated->item_agenda_hora, 'item_agenda_item'=>$itemUpdated->item_agenda_item, 'item_agenda_descripcion'=>$itemUpdated->item_agenda_descripcion]);
            if($isUpdated){
                return response()->json(
                    ['status'=> 'OK', 'message'=>'Item actualizado'],200
                );
            }else{
                return response()->json(
                    ['status'=> 'KO', 'message'=>'Item no actualizado'],500
                );
            }


        }


    }

    public function deleteItemAgenda($itemAgendaId){

        $itemToDelete= Items_agenda::where('id',$itemAgendaId)->delete();
        if(!$itemToDelete){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe el evento en la agenda'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$itemToDelete],200
        );

    }


    public function getItemAgendaEnFecha($itemAgendaId,$fecha){

        $itemsFecha = Items_agenda::where('item_agenda_agenda_id',$itemAgendaId)->where('item_agenda_fecha','=',$fecha)->get();
        if(!$itemsFecha){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existen  eventos en la agenda en esa fecha'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$itemsFecha],200
        );
        

    }




    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Items_agenda  $items_agenda
     * @return \Illuminate\Http\Response
     */
    public function show(Items_agenda $items_agenda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Items_agenda  $items_agenda
     * @return \Illuminate\Http\Response
     */
    public function edit(Items_agenda $items_agenda)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Items_agenda  $items_agenda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Items_agenda $items_agenda)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Items_agenda  $items_agenda
     * @return \Illuminate\Http\Response
     */
    public function destroy(Items_agenda $items_agenda)
    {
        //
    }
}
