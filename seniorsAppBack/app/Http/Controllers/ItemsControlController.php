<?php

namespace App\Http\Controllers;


use App\Models\Items_control;
use App\Models\Medicamentos;
use Illuminate\Http\Request;

class ItemsControlController extends Controller
{

    public function insertAllItemsToControl($controlId,$horarioId){

        $medicamentos= Medicamentos::where('medicamento_horario_id',$horarioId)->get();


        foreach ($medicamentos as $medicamento) {

            $medic = new Items_control();
            $medic->items_controles_control_medicaion_id = $controlId;
            $medic->items_controles_medicaion_id = $medicamento->id;
            $medic->items_controles_verificacion = false;
            $medic->save();
            
        }

    }


    public function updateOneItemsToControl($controlId,$medicamentoId,$validacion){

            $medic = new Items_control();
            $medic->id=$itemControlId;
            $medic->items_controles_verificacion = $validacion;
            $medic=Items_control::where('items_controles_medicaion_id', $medicamentoId)->where('items_controles_control_medicaion_id', $controlId)->update(['items_controles_verificacion'=>$medic->items_controles_verificacion]);
            if($isUpdated){
                return response()->json(
                    ['status'=> 'OK', 'message'=>'Validacion de medicamento actualizada'],200
                );
            }else{
                return response()->json(
                    ['status'=> 'KO', 'message'=>'Validacion de medicamento no actualizada'],500
                );
            }
            
        

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Items_control  $items_control
     * @return \Illuminate\Http\Response
     */
    public function show(Items_control $items_control)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Items_control  $items_control
     * @return \Illuminate\Http\Response
     */
    public function edit(Items_control $items_control)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Items_control  $items_control
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Items_control $items_control)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Items_control  $items_control
     * @return \Illuminate\Http\Response
     */
    public function destroy(Items_control $items_control)
    {
        //
    }
}
