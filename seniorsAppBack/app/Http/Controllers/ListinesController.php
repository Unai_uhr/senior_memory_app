<?php

namespace App\Http\Controllers;

use App\Models\Listines;
use Illuminate\Http\Request;

class ListinesController extends Controller
{

    public function createListin(Request $request,$cuenta)
    {
        $listin = new Listines();
        $listin->listin_cuenta_id = $cuenta;
        $listin->save();
        return response()->json($listin);


    }


    public function showOne($cuenta)
    {
        $listin= Listines::where('listin_cuenta_id',$cuenta)->first();
        if(!$listin){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe listin de telefonos en esa cuenta'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$listin],200
        );
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Listines  $listines
     * @return \Illuminate\Http\Response
     */
    public function show(Listines $listines)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Listines  $listines
     * @return \Illuminate\Http\Response
     */
    public function edit(Listines $listines)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Listines  $listines
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Listines $listines)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Listines  $listines
     * @return \Illuminate\Http\Response
     */
    public function destroy(Listines $listines)
    {
        //
    }
}
