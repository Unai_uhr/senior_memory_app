<?php

namespace App\Http\Controllers;

use App\Models\Medicamentos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;



class MedicamentosController extends Controller
{

    public function createMedicamento(Request $request,$horarioId)
    {
        $medicamento = new Medicamentos();
        $medicamento->medicamento_horario_id = $horarioId;
        $medicamento->medicamento_franja_id=$request->medicamento_franja_id;
        $medicamento->medicamento_nombre=$request->medicamento_nombre;
        $medicamento->medicamento_cantidad=$request->medicamento_cantidad;
        $medicamento->save();
        return response()->json($medicamento);

    }

    public function showOne($medicamentoId)
    {
        $medicamento= Medicamentos::where('id',$medicamentoId)->first();
        if(!$medicamento){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe el medicamento en el horario'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$medicamento],200
        );
    }

    public function showAll($horarioId)
    {
        $medicamento= Medicamentos::where('medicamento_horario_id',$horarioId)->get();
        if(!$medicamento){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No hay medicamentos en el horario'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$medicamento],200
        );
    }


    public function updateMedicamento(Request $request, $medicamentoId)
    {
        $medicamento= Medicamentos::where('id',$medicamentoId)->first();
        if(!$medicamento){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe el medicamento en el horario'
                    ])
                ],404
            );
        }else{
            $medicamentoUpdated=new Medicamentos();
            $medicamentoUpdated->id=$medicamentoId;
            $medicamentoUpdated->medicamento_horario_id=$medicamento->medicamento_horario_id;
            $medicamentoUpdated->medicamento_franja_id=$request->medicamento_franja_id;
            $medicamentoUpdated->medicamento_nombre=$request->medicamento_nombre;
            $medicamentoUpdated->medicamento_cantidad=$request->medicamento_cantidad;
            //$isUpdated=Medicamentos::where('id',$medicamentoUpdated->id)->update($medicamentoUpdated);
            $isUpdated=DB::table('medicamentos')->where('id', $medicamentoUpdated->id)->update(['medicamento_franja_id'=>$medicamentoUpdated->medicamento_franja_id,'medicamento_nombre'=>$medicamentoUpdated->medicamento_nombre,'medicamento_cantidad'=>$medicamentoUpdated->medicamento_cantidad]);
            if($isUpdated){
                return response()->json(
                    ['status'=> 'OK', 'message'=>'Medicamento actualizado'],200
                );
            }else{
                return response()->json(
                    ['status'=> 'KO', 'message'=>'Medicamento no actualizado'],500
                );
            }


        }


    }

    public function deleteMedicamento($medicamentoId){

        $medicamento= Medicamentos::where('id',$medicamentoId)->delete();
        if(!$medicamento){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe el medicamento en el horario'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$medicamento],200
        );

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Medicamentos  $medicamentos
     * @return \Illuminate\Http\Response
     */
    public function show(Medicamentos $medicamentos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Medicamentos  $medicamentos
     * @return \Illuminate\Http\Response
     */
    public function edit(Medicamentos $medicamentos)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Medicamentos  $medicamentos
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Medicamentos $medicamentos)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Medicamentos  $medicamentos
     * @return \Illuminate\Http\Response
     */
    public function destroy(Medicamentos $medicamentos)
    {
        //
    }
}
