<?php

namespace App\Http\Controllers;

use App\Models\Numeros_tel;
use Illuminate\Http\Request;

class NumerosTelController extends Controller
{

    public function createTel(Request $request,$listinId)
    {
        $numTel = new Numeros_tel();
        $numTel->numero_tel_listin_id = $listinId;
        $numTel->numero_tel_nombre=$request->numero_tel_nombre;
        $numTel->numero_tel_imagen=$request->numero_tel_imagen;
        $numTel->numero_tel_numero=$request->numero_tel_numero;
        $numTel->save();
        return response()->json($numTel);

    }

    public function showOne($telId)
    {
        $numTel= Numeros_tel::where('id',$telId)->first();
        if(!$numTel){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe el numero de telefono en el listin'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$numTel],200
        );
    }

    public function showAll($listinId)
    {
        $numTel= Numeros_tel::where('numero_tel_listin_id',$listinId)->get();
        if(!$numTel){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No hay telefonos en el listin'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$numTel],200
        );
    }


    public function updateTel(Request $request, $telId)
    {
        $numTel= Numeros_tel::where('id',$telId)->first();
        if(!$numTel){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe el telefono en el listin'
                    ])
                ],404
            );
        }else{
            $numTelUpdated=new Numeros_tel();
            $numTelUpdated->id=$telId;
            $numTelUpdated->numero_tel_listin_id=$numTel->numero_tel_listin_id;
            $numTelUpdated->numero_tel_imagen=$request->numero_tel_imagen;
            $numTelUpdated->numero_tel_nombre=$request->numero_tel_nombre;
            $numTelUpdated->numero_tel_numero=$request->numero_tel_numero;
            $isUpdated=Numeros_tel::where('id',$numTelUpdated->id)->update(['numero_tel_imagen'=>$numTelUpdated->numero_tel_imagen, 'numero_tel_nombre'=>$numTelUpdated->numero_tel_nombre ,'numero_tel_numero'=>$numTelUpdated->numero_tel_numero]);
            if($isUpdated){
                return response()->json(
                    ['status'=> 'OK', 'message'=>'Telefono actualizada'],200
                );
            }else{
                return response()->json(
                    ['status'=> 'KO', 'message'=>'Telefono no actualizada'],500
                );
            }


        }


    }


    public function deleteTelAgenda($numTelId){

        $itemToDelete= Numeros_tel::where('id',$numTelId)->delete();
        if(!$itemToDelete){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe el número de teléfono del listin'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$itemToDelete],200
        );

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Numeros_tel  $numeros_tel
     * @return \Illuminate\Http\Response
     */
    public function show(Numeros_tel $numeros_tel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Numeros_tel  $numeros_tel
     * @return \Illuminate\Http\Response
     */
    public function edit(Numeros_tel $numeros_tel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Numeros_tel  $numeros_tel
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Numeros_tel $numeros_tel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Numeros_tel  $numeros_tel
     * @return \Illuminate\Http\Response
     */
    public function destroy(Numeros_tel $numeros_tel)
    {
        //
    }
}
