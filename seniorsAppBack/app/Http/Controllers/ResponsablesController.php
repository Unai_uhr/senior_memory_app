<?php

namespace App\Http\Controllers;

use App\Models\Responsables;
use Illuminate\Http\Request;

class ResponsablesController extends Controller
{
    public function createResponsable(Request $request,$cuenta)
    {
        $responsable = new Responsables();
        $responsable->responsable_cuenta_id = $cuenta;
        $responsable->responsable_nombre = $request->responsable_nombre;
        $responsable->save();
        return response()->json($responsable);


    }

    public function showAll()
    {
        $responsable= Responsables::all();
        if(!$responsable){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No hay usuarios responsables registrados'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$responsable],200
        );
    }

    public function showOne($cuenta)
    {
        $responsable= Responsables::where('responsable_cuenta_id',$cuenta)->first();
        if(!$responsable){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe el usuario responsable en esa cuenta'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$responsable],200
        );
    }
    public function updateResponsable(Request $request, $cuentaId)
    {
        $responsable= Responsables::where('responsable_cuenta_id',$cuentaId)->first();
        if(!$responsable){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe responsable en esta cuenta '
                    ])
                ],404
            );
        }else{
            $responsableUpdated=new Responsables();
            $responsableUpdated->id=$responsable->id;
            $responsableUpdated->responsable_cuenta_id=$cuentaId;
            $responsableUpdated->responsable_nombre=$request->responsable_nombre;
            $isUpdated=Responsables::where('id',$responsableUpdated->id)->update($responsableUpdated);
            if($isUpdated){
                return response()->json(
                    ['status'=> 'OK', 'message'=>'Usuario responsable actualizada'],200
                );
            }else{
                return response()->json(
                    ['status'=> 'KO', 'message'=>'Usuario responsable no actualizada'],500
                );
            }


        }


    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Responsables  $responsables
     * @return \Illuminate\Http\Response
     */
    public function show(Responsables $responsables)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Responsables  $responsables
     * @return \Illuminate\Http\Response
     */
    public function edit(Responsables $responsables)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Responsables  $responsables
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Responsables $responsables)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Responsables  $responsables
     * @return \Illuminate\Http\Response
     */
    public function destroy(Responsables $responsables)
    {
        //
    }
}
