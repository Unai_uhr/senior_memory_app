<?php

namespace App\Http\Controllers;

use App\Models\Usuarios;
use Illuminate\Http\Request;

class UsuariosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createUsuario(Request $request,$cuenta)
    {
        $usuario = new Usuarios();
        $usuario->usuario_cuenta_id = $cuenta;
        $usuario->usuario_nombre = $request->usuario_nombre;
        $usuario->usuario_apellidos = $request->usuario_apellidos;
        $usuario->usuario_fecha_nacimiento = $request->usuario_fecha_nacimiento;
        $usuario->usuario_genero = $request->usuario_genero;
        $usuario->usuario_email = $request->usuario_email;
        $usuario->save();
        return response()->json($usuario);


    }

    public function showAll()
    {
        $usuario= Usuarios::all();
        if(!$usuario){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No hay usuarios registrados'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$usuario],200
        );
    }

    public function showOne($cuenta)
    {
        $usuario= Usuarios::where('usuario_cuenta_id',$cuenta)->first();
        if(!$usuario){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe el usuario  en esa cuenta'
                    ])
                ],404
            );
        }
        return response()->json(
            ['status'=> 'OK', 'data'=>$usuario],200
        );
    }
    public function updateUsuario(Request $request, $cuentaId)
    {
        $usuario= Usuarios::where('usuario_cuenta_id',$cuentaId)->first();
        if(!$usuario){
            return response()->json(
                [
                    'error'=>Array([
                        'code'=> 404,
                        'message' => 'No existe la cuenta con ese correo electronico'
                    ])
                ],404
            );
        }else{
            $usuarioUpdated=new Usuarios();
            $usuarioUpdated->id=$usuario->id;
            $usuarioUpdated->usuario_cuenta_id=$cuentaId;
            $usuarioUpdated->usuario_nombre=$request->usuario_nombre;
            $usuarioUpdated->usuario_apellidos=$request->usuario_apellidos;
            $usuarioUpdated->usuario_fecha_nacimiento=$request->usuario_fecha_nacimiento;
            $usuarioUpdated->usuario_genero=$request->usuario_genero;
            $usuarioUpdated->usuario_email=$request->usuario_email;
            $isUpdated=Usuarios::where('id',$usuarioUpdated->id)->update($usuarioUpdated);
            if($isUpdated){
                return response()->json(
                    ['status'=> 'OK', 'message'=>'Usuario actualizada'],200
                );
            }else{
                return response()->json(
                    ['status'=> 'KO', 'message'=>'Usuario no actualizada'],500
                );
            }


        }


    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function show(Usuarios $usuarios)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function edit(Usuarios $usuarios)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Usuarios $usuarios)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Usuarios  $usuarios
     * @return \Illuminate\Http\Response
     */
    public function destroy(Usuarios $usuarios)
    {
        //
    }
}
