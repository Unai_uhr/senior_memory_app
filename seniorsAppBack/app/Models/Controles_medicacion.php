<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Controles_medicacion extends Model
{
    use HasFactory;

    protected $fillable = [
        'controles_medicacion_horario_id',
        'controles_medicacion_fecha'
    ];
}
