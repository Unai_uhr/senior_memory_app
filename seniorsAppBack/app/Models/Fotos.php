<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fotos extends Model
{
    use HasFactory;

    protected $fillable = [
        'foto_album_foto_id',
        'foto_imagen',
        'foto_comentario'
    ];
}
