<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Franjas_horarias extends Model
{
    use HasFactory;

    protected $fillable = [
        'franja_horaria_franja'
    ];
    
}
