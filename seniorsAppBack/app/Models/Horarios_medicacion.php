<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Horarios_medicacion extends Model
{
    use HasFactory;

    protected $fillable = [
        'horario_medicacion_cuenta_id'
    ];
}
