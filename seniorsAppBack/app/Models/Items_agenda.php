<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Items_agenda extends Model
{
    use HasFactory;

    protected $fillable = [
        'item_agenda_agenda_id',
        'item_agenda_fecha',
        'item_agenda_hora',
        'item_agenda_item',
        'item_agenda_descripcion'
    ];
}
