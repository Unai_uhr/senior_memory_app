<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Items_control extends Model
{
    use HasFactory;

    protected $fillable = [
        'items_controles_control_medicaion_id',
        'items_controles_medicaion_id',
        'items_controles_verificacion'
    ];

}
