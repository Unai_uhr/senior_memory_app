<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Listines extends Model
{
    use HasFactory;

    protected $fillable = [
        'listin_cuenta_id'
    ];
}
