<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medicamentos extends Model
{
    use HasFactory;


    protected $fillable = [
        'medicamento_horario_id',
        'medicamento_franja_id',
        'medicamento_nombre',
        'medicamento_cantidad'
    ];
}
