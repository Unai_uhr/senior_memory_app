<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Numeros_tel extends Model
{
    use HasFactory;

    protected $fillable = [
        'numero_tel_listin_id',
        'numero_tel_nombre',
        'numero_tel_imagen',
        'numero_tel_numero'
    ];
}
