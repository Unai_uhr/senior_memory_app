<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuarios extends Model
{
    use HasFactory;

    protected $fillable = [
        'usuario_cuenta_id',
        'usuario_nombre',
        'usuario_apellidos',
        'usuario_fecha_nacimiento',
        'usuario_genero',
        'usuario_email'
    ];
}
