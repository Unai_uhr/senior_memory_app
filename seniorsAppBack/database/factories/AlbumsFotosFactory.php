<?php

namespace Database\Factories;

use App\Models\Albums_fotos;
use Illuminate\Database\Eloquent\Factories\Factory;

class AlbumsFotosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Albums_fotos::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
