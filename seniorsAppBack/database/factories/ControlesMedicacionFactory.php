<?php

namespace Database\Factories;

use App\Models\Controles_medicacion;
use Illuminate\Database\Eloquent\Factories\Factory;

class ControlesMedicacionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Controles_medicacion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
