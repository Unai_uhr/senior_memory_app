<?php

namespace Database\Factories;

use App\Models\Fotos;
use Illuminate\Database\Eloquent\Factories\Factory;

class FotosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Fotos::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
