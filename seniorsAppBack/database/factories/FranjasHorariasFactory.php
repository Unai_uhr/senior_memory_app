<?php

namespace Database\Factories;

use App\Models\Franjas_horarias;
use Illuminate\Database\Eloquent\Factories\Factory;

class FranjasHorariasFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Franjas_horarias::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
