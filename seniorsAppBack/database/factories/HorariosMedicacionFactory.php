<?php

namespace Database\Factories;

use App\Models\Horarios_medicacion;
use Illuminate\Database\Eloquent\Factories\Factory;

class HorariosMedicacionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Horarios_medicacion::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
