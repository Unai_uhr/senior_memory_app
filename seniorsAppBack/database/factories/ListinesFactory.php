<?php

namespace Database\Factories;

use App\Models\Listines;
use Illuminate\Database\Eloquent\Factories\Factory;

class ListinesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Listines::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
