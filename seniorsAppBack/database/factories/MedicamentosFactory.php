<?php

namespace Database\Factories;

use App\Models\Medicamentos;
use Illuminate\Database\Eloquent\Factories\Factory;

class MedicamentosFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Medicamentos::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
