<?php

namespace Database\Factories;

use App\Models\Puntuaciones;
use Illuminate\Database\Eloquent\Factories\Factory;

class PuntuacionesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Puntuaciones::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
