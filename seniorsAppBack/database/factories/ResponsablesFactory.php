<?php

namespace Database\Factories;

use App\Models\Responsables;
use Illuminate\Database\Eloquent\Factories\Factory;

class ResponsablesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Responsables::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
