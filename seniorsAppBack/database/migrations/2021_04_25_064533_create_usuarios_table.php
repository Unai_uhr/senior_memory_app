<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('usuario_cuenta_id');
            $table->foreign('usuario_cuenta_id')->references('id')->on('users');
            $table->string('usuario_nombre');
            $table->string('usuario_apellidos');
            $table->date('usuario_fecha_nacimiento');
            $table->char('usuario_genero');
            $table->string('usuario_email');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
