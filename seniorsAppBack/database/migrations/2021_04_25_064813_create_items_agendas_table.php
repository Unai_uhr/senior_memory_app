<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsAgendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_agendas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('item_agenda_agenda_id');
            $table->foreign('item_agenda_agenda_id')->references('id')->on('agendas');
            $table->date('item_agenda_fecha');
            $table->time('item_agenda_hora');
            $table->string('item_agenda_item');
            $table->string('item_agenda_descripcion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_agendas');
    }
}
