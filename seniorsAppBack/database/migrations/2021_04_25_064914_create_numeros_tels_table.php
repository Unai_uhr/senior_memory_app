<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNumerosTelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('numeros_tels', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('numero_tel_listin_id');
            $table->foreign('numero_tel_listin_id')->references('id')->on('listines');
            $table->string('numero_tel_nombre');
            $table->string('numero_tel_imagen');
            $table->string('numero_tel_numero');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('numeros_tels');
    }
}
