<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicamentosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicamentos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('medicamento_horario_id');
            $table->foreign('medicamento_horario_id')->references('id')->on('horarios_medicaciones');
            $table->unsignedBigInteger('medicamento_franja_id');
            $table->foreign('medicamento_franja_id')->references('id')->on('franjas_horarias');
            $table->string('medicamento_nombre');
            $table->string('medicamento_cantidad');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicamentos');
    }
}
