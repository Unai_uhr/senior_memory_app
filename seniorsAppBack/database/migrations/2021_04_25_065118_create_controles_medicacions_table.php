<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateControlesMedicacionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('controles_medicaciones', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('controles_medicacion_horario_id');
            $table->foreign('controles_medicacion_horario_id')->references('id')->on('horarios_medicaciones');
            $table->date('controles_medicacion_fecha');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('controles_medicacions');
    }
}
