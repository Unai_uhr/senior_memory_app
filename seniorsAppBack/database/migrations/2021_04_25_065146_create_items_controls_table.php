<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items_controls', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('items_controles_control_medicaion_id');
            $table->foreign('items_controles_control_medicaion_id')->references('id')->on('controles_medicaciones');

            $table->unsignedBigInteger('items_controles_medicaion_id');
            $table->foreign('items_controles_medicaion_id')->references('id')->on('medicamentos');
            
            $table->boolean('items_controles_verificacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items_controls');
    }
}
