<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePuntuacionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('puntuaciones', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('puntuacion_cuenta_id');
            $table->foreign('puntuacion_cuenta_id')->references('id')->on('users');

            $table->unsignedBigInteger('puntuacion_juego_id');
            $table->foreign('puntuacion_juego_id')->references('id')->on('juegos');

            $table->date('puntuacion_fecha');
            $table->integer('puntuacion_puntuacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('puntuaciones');
    }
}
