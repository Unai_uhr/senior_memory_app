<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\CuentasController;
use \App\Http\Controllers\UsuariosController;
use \App\Http\Controllers\ResponsablesController;
use \App\Http\Controllers\AlbumsFotosController;
use \App\Http\Controllers\FotosController;
use \App\Http\Controllers\ListinesController;
use \App\Http\Controllers\NumerosTelController;
use \App\Http\Controllers\AgendasController;
use \App\Http\Controllers\ItemsAgendaController;
use \App\Http\Controllers\HorariosMedicacionController;
use \App\Http\Controllers\MedicamentosController;
use \App\Http\Controllers\ControlesMedicacionController;
use \App\Http\Controllers\ItemsControlController;
use Illuminate\Support\Facades\Auth;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


//RUTAS CON CUENTAS
Route::get('/cuentas/all',[CuentasController::class, 'showAll']);
Route::get('/cuentas/getone/{email}',[CuentasController::class, 'showOne']);
Route::post('/cuentas/createcuenta/',[CuentasController::class, 'createCuenta']);
Route::put('/cuentas/updatecuenta/{id}',[CuentasController::class, 'updateCuenta']);

//RUTAS PARA LOGIN

Route::post('login', [CuentasController::class,'login']);
Route::post('register', [CuentasController::class,'register']);
Route::group(['middleware' => 'auth:api'], function() {
    Route::post('details', [CuentasController::class, 'details']);
});


//RUTAS CON USUARIOS
Route::get('/usuarios/all',[UsuariosController::class, 'showAll']);
Route::get('/usuarios/getone/{cuentaId}',[UsuariosController::class, 'showOne']);
Route::post('/usuarios/create/{cuentaId}',[UsuariosController::class, 'createUsuario']);
Route::put('/usuarios/update/{usuarioId}',[UsuariosController::class, 'updateUsuario']);


//RUTAS CON RESPONSABLES
Route::get('/responsables/all',[ResponsablesController::class, 'showAll']);
Route::get('/responsables/getone/{cuentaId}',[ResponsablesController::class, 'showOne']);
Route::post('/responsables/create/{cuentaId}',[ResponsablesController::class, 'createResponsable']);
Route::put('/responsables/update/{responsableId}',[ResponsablesController::class, 'updateResponsable']);


//RUTAS CON ALBUM DE FOTOS
Route::get('/album-fotos/getone/{cuentaId}',[AlbumsFotosController::class, 'showOne']);
Route::post('/album-fotos/create/{cuentaId}',[AlbumsFotosController::class, 'createAlbum']);

Route::get('/fotos/all/{albumId}',[FotosController::class, 'showAll']);
Route::get('/fotos/getone/{fotoId}',[FotosController::class, 'showOne']);
Route::post('/fotos/create/{albumId}',[FotosController::class, 'createFoto']);
Route::put('/fotos/update/{fotoId}',[FotosController::class, 'updateFoto']);
Route::delete('/fotos/delete/{fotoId}',[FotosController::class, 'deleteFotoAlbum']);

//RUTAS CON LISTIN TELEFONOS
Route::get('/listin-tel/getone/{cuentaId}',[ListinesController::class, 'showOne']);
Route::post('/listin-tel/create/{cuentaId}',[ListinesController::class, 'createListin']);

Route::get('/tel/all/{listinId}',[NumerosTelController::class, 'showAll']);
Route::get('/tel/getone/{telId}',[NumerosTelController::class, 'showOne']);
Route::post('/tel/create/{listinId}',[NumerosTelController::class, 'createTel']);
Route::put('/tel/update/{telId}',[NumerosTelController::class, 'updateTel']);
Route::delete('/tel/delete/{telId}',[NumerosTelController::class, 'deleteTelAgenda']);


//RUTAS CON AGENDAS
Route::get('/agenda/getone/{cuentaId}',[AgendasController::class, 'showOne']);
Route::post('/agenda/create/{cuentaId}',[AgendasController::class, 'createAgenda']);

Route::get('/agenda-item/all/{agendaId}/{fecha}',[ItemsAgendaController::class, 'showAll']);
Route::get('/agenda-item/getone/{itemId}',[ItemsAgendaController::class, 'showOne']);
Route::post('/agenda-item/create/{agendaId}',[ItemsAgendaController::class, 'createItemAgenda']);
Route::put('/agenda-item/update/{itemId}',[ItemsAgendaController::class, 'updateItemAgenda']);
Route::delete('/agenda-item/delete/{itemId}',[ItemsAgendaController::class, 'deleteItemAgenda']);

Route::get('/agenda-item/all-fecha/{agendaId}/{fecha}',[ItemsAgendaController::class, 'getItemAgendaEnFecha']);


//RUTAS CON MEDICACIOM
Route::get('/horario/getone/{cuentaId}',[HorariosMedicacionController::class, 'showOne']);
Route::post('/horario/create/{cuentaId}',[HorariosMedicacionController::class, 'createHorario']);

Route::get('/medicacion/all/{horarioId}',[MedicamentosController::class, 'showAll']);
Route::get('/medicacion/getone/{medicamentoId}',[MedicamentosController::class, 'showOne']);
Route::post('/medicacion/create/{horarioId}',[MedicamentosController::class, 'createMedicamento']);
Route::put('/medicacion/update/{medicamentoId}',[MedicamentosController::class, 'updateMedicamento']);
Route::delete('/medicacion/delete/{medicamentoId}',[MedicamentosController::class, 'deleteMedicamento']);

//RUTAS CONTROLES MEDICACION
Route::post('/control-medicacion/create/{horarioId}/{fecha}',[ControlesMedicacionController::class, 'createControlMedicacion']);//OK -> para crear si no existe el control de ese dia
Route::get('/control-medicacion/get-one/{horarioId}/{fecha}',[ControlesMedicacionController::class, 'getOneControlMedicacion']);
Route::get('/control-medicacion/get-all/{horarioId}',[ControlesMedicacionController::class, 'getAllControlMedicacion']);//OK -> para mostra al responsable la info

Route::post('/items-control-medicacion/insert-all/{controlId}/{horarioId}',[ItemsControlController::class, 'insertAllItemsToControl']);//OK -> insertar todos los medicamentos del dia en el control del dia
Route::put('/items-control-medicacion/update-item/{controlId}/{medicamentoId}/{validacion}',[ItemsControlController::class, 'updateOneItemsToControl']);//OK -> al presionar el abuelo el toogle

