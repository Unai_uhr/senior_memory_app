import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'configuracion',
    loadChildren: () => import('./pages/configuracion/configuracion.module').then( m => m.ConfiguracionPageModule)
  },
  {
    path: 'add-medicamento',
    loadChildren: () => import('./pages/add-medicamento/add-medicamento.module').then( m => m.AddMedicamentoPageModule)
  },
  {
    path: 'inicio-configuracion',
    loadChildren: () => import('./pages/inicio-configuracion/inicio-configuracion.module').then( m => m.InicioConfiguracionPageModule)
  },
  {
    path: 'update-medicamento',
    loadChildren: () => import('./pages/update-medicamento/update-medicamento.module').then( m => m.UpdateMedicamentoPageModule)
  },
  {
    path: 'add-agenda',
    loadChildren: () => import('./pages/add-agenda/add-agenda.module').then( m => m.AddAgendaPageModule)
  },
  {
    path: 'update-agenda',
    loadChildren: () => import('./pages/update-agenda/update-agenda.module').then( m => m.UpdateAgendaPageModule)
  },
  {
    path: 'add-foto-album',
    loadChildren: () => import('./pages/add-foto-album/add-foto-album.module').then( m => m.AddFotoAlbumPageModule)
  },
  {
    path: 'update-foto-album',
    loadChildren: () => import('./pages/update-foto-album/update-foto-album.module').then( m => m.UpdateFotoAlbumPageModule)
  },
  {
    path: 'add-numero-listin',
    loadChildren: () => import('./pages/add-numero-listin/add-numero-listin.module').then( m => m.AddNumeroListinPageModule)
  },
  {
    path: 'update-numero-listin',
    loadChildren: () => import('./pages/update-numero-listin/update-numero-listin.module').then( m => m.UpdateNumeroListinPageModule)
  },
  {
    path: 'loading',
    loadChildren: () => import('./pages/loading/loading.module').then( m => m.LoadingPageModule)
  },
  {
    path: 'reports',
    loadChildren: () => import('./pages/reports/reports.module').then( m => m.ReportsPageModule)
  },
  {
    path: 'simon',
    loadChildren: () => import('./juegos/simon/simon.module').then( m => m.SimonPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
