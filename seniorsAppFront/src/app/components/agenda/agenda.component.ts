import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfiguracionDataService } from 'src/app/services/configuracion-data.service';
import { Storage } from '@ionic/storage-angular';
import { AlertController } from '@ionic/angular';


const baseURL="https://unai-hernandez-7e3.alwaysdata.net/memory_senior/api/";
const options = {
  headers: {'Content-Type':'application/json'}
}

@Component({
  selector: 'app-agenda',
  templateUrl: './agenda.component.html',
  styleUrls: ['./agenda.component.scss'],
})
export class AgendaComponent implements OnInit {

  agendaItem:any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private configuracionDataService: ConfiguracionDataService,
    private storage:Storage,
    public alertController: AlertController,
  ) { }

  ngOnInit() {

    this.configuracionDataService.mostrarItemsAgenda();
    this.mostrarItemsAgenda();
  }


  addItemAgenda(){
    this.router.navigate(['add-agenda'])
  }

  async mostrarItemsAgenda(){

    //Busco en la base de datos e inserto en Storage
    await this.configuracionDataService.mostrarItemsAgenda();
    
    //Recojo del Storage
    this.agendaItem=await this.storage.get('listaAgenda');

    
  }

  editarItemAgenda(agenda){

    this.router.navigate(['update-agenda', agenda]);
    

  }

  async borrarItemAgenda(agenda:any){

    console.log("BORRAR" ,agenda)

      const alert = await this.alertController.create({
        cssClass: 'alert-class',
        header: 'Alerta',
        message: `¿Seguro que quieres borrar el evento ${agenda.item_agenda_item} de la agenda?`,
        buttons: [
          {
            text: 'CANCELAR',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              //console.log('Confirm Cancel: blah');
              
            }
          }, {
            text: 'CONFIRMAR',
            handler: () => {
              console.log('Confirm Okay');
              this.borrar(agenda);
            }
          }
        ]
      });
  
      await alert.present();
  }

  async borrar(agenda:any){
    let borrado= await (await fetch(`${baseURL}agenda-item/delete/${agenda['id']}`, {
      method: "DELETE"
    })).json();
    console.log("BORRAR", agenda)
     //this.ngOnInit();
    await this.configuracionDataService.mostrarItemsAgenda();
    await this.mostrarItemsAgenda();

  }

 

}
