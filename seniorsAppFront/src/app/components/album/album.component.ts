import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfiguracionDataService } from 'src/app/services/configuracion-data.service';
import { Storage } from '@ionic/storage-angular';
import { AlertController } from '@ionic/angular';


const baseURL="https://unai-hernandez-7e3.alwaysdata.net/memory_senior/api/";
const options = {
  headers: {'Content-Type':'application/json'}
}

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.scss'],
})
export class AlbumComponent implements OnInit {

  albumFotos:any;


  

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private configuracionDataService: ConfiguracionDataService,
    private storage:Storage,
    public alertController: AlertController,
  ) { }

  ngOnInit() {
    this.configuracionDataService.mostrarFotos();
    this.mostrarFotos()

  }

  addFoto(){
    this.router.navigate(['add-foto-album'])
  }

  async mostrarFotos(){
    await this.configuracionDataService.mostrarFotos();

    this.albumFotos=await this.storage.get('listaFotos');

  }

  async editarFoto(fotoToUpdate:any){

    this.router.navigate(['update-foto-album', fotoToUpdate]);

  }

  async borrarFoto(fotoToDelete: any){
    
    const alert = await this.alertController.create({
      cssClass: 'alert-class',
      header: 'Alerta',
      message: `¿Seguro que quieres borrar la foto del álbum?`,
      buttons: [
        {
          text: 'CANCELAR',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            //console.log('Confirm Cancel: blah');
            
          }
        }, {
          text: 'CONFIRMAR',
          handler: () => {
            console.log('Confirm Okay');
            this.borrar(fotoToDelete);
          }
        }
      ]
    });

    await alert.present();
}

async borrar(fotoToDelete:any){
  let borrado= await (await fetch(`${baseURL}fotos/delete/${fotoToDelete['id']}`, {
    method: "DELETE"
  })).json();
  console.log("BORRAR", fotoToDelete)
   //this.ngOnInit();
  await this.configuracionDataService.mostrarFotos();
  await this.mostrarFotos();

}
  

}
