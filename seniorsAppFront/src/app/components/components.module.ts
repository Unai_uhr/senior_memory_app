import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { HeaderComponent } from './header/header.component';
import { DatosPersonalesComponent } from './datos-personales/datos-personales.component';
import { FormsModule } from '@angular/forms';
import { HorariosComponent } from './horarios/horarios.component';
import { AlbumComponent } from './album/album.component';
import { AgendaComponent } from './agenda/agenda.component';
import { ListinComponent } from './listin/listin.component';
import { HeaderRegisterComponent } from './header-register/header-register.component';



@NgModule({
  declarations: [HeaderRegisterComponent,HeaderComponent,DatosPersonalesComponent,HorariosComponent,AlbumComponent,AgendaComponent,ListinComponent],
  imports: [CommonModule, IonicModule,FormsModule],
  exports: [HeaderRegisterComponent,HeaderComponent,DatosPersonalesComponent,HorariosComponent,AlbumComponent,AgendaComponent,ListinComponent],
})
export class ComponentsModule { }