import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Storage } from '@ionic/storage-angular';
import { ActivatedRoute, Router } from '@angular/router';
import { Login, Register ,InfoCuenta ,DatosFormulario} from './../../interfaces/interfaces';
import { ConfiguracionPage } from './../../pages/configuracion/configuracion.page'
import { ConfiguracionDataService } from '../../services/configuracion-data.service';

@Component({
  selector: 'app-datos-personales',
  templateUrl: './datos-personales.component.html',
  styleUrls: ['./datos-personales.component.scss'],
})
export class DatosPersonalesComponent implements OnInit {
  route: ActivatedRoute
  router: Router

  datosFormulario:DatosFormulario={
    id:0,
    nombreRes:"",
    cuenta_email:"",
    nombre:"",
    apellido:"",
    nacimiento:"",
    genero:"",
    email:""}

  constructor(
    private storage:Storage,
    private configuracionDataService: ConfiguracionDataService,
  ) { 
    this.cargarDatosPersonales();
  }

  ngOnInit() {

    //Cargamos datos del usuario que exista
    this.cargarDatosPersonales();

  }

  ionViewWillEnter(){
    this.cargarDatosPersonales();
  }

  async cargarDatosPersonales(){

   
      this.datosFormulario=await this.storage.get('infoCuenta')
      //console.log(this.datosFormulario);
    
    
  }

  guardarDatosPeronales(){
    //console.log(this.datosFormulario);

    let isSuccess=this.configuracionDataService.guardarDatosPersonales(this.datosFormulario);
  }

}
