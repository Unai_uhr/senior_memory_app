import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header-register',
  templateUrl: './header-register.component.html',
  styleUrls: ['./header-register.component.scss'],
})
export class HeaderRegisterComponent implements OnInit {

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {}

  goHome(){
    this.router.navigate(['login'])
  }

}
