import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { ActivatedRoute, NavigationExtras } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Login, Register ,InfoCuenta, DatosFormulario } from '../../interfaces/interfaces';


const baseURL="https://unai-hernandez-7e3.alwaysdata.net/memory_senior/api/";

const options = {
  headers: {'Content-Type':'application/json'}
}


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  nombre:string;
  nombreResponsable:string;
  emailCuenta:string;

  constructor(
    private router: Router,
    public alertController: AlertController,
    private storage:Storage,
    private http: HttpClient,
  ) { 

    this.storage.get('infoCuenta').then(async (result) => {
      this.nombre=await result.usuario_nombre;
      this.nombreResponsable=await result.nombreRes;
      this.emailCuenta=await result.cuenta_email;
    });
  }

  ngOnInit() {}


  async goConfig(){

    const alert = await this.alertController.create({
      cssClass: 'alert-class',
      header: 'Acceso a configuración',
      message: `${this.nombre}, pulsa Salir. Aquí entrará ${this.nombreResponsable} para configurar la aplicación`,
      inputs: [
        {
          name: 'password_input',
          type: 'password',
          placeholder: `${this.nombreResponsable}, introduce la contraseña para acceder`
        }],
      buttons: [
        {
          text: 'SALIR',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          
            
          }
        }, {
          text: 'ACCEDER',
          handler: (alertData) => {
            this.accederConfig(alertData.password_input);
        }
        }
      ]
    });

    await alert.present();


    //
    
  }
  goHome(){
    this.router.navigate(['home'])
  }

  accederConfig(strPassword){


    const datos = {
      cuenta_email:this.emailCuenta,
      password : strPassword
    };

    this.http.post(`${baseURL}login` , JSON.stringify(datos),options).subscribe(async (json: Login)=> 
    {
      const parametro={
        segmento:'datosPersonales'
      }
      this.router.navigate(['configuracion',parametro]);

    },(error)=>{
      this.presentAlert();
      return "error";
      
      

    });

  }

  async presentAlert() {
    let alert =await this.alertController.create({
      cssClass: 'alert-class',
      header: 'Error de autentificación',
      message: `Contraseña erronea`,
      buttons: [
        {
          text: 'SALIR',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          
            
          }
        }
      ]
    });
    ( alert).present();
  }

}
