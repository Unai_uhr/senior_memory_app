import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router, RouterModule } from '@angular/router';
import { ConfiguracionDataService } from 'src/app/services/configuracion-data.service';
import { Storage } from '@ionic/storage-angular';
import { AlertController } from '@ionic/angular';


const baseURL="https://unai-hernandez-7e3.alwaysdata.net/memory_senior/api/";

const options = {
  headers: {'Content-Type':'application/json'}
}

@Component({
  selector: 'app-horarios',
  templateUrl: './horarios.component.html',
  styleUrls: ['./horarios.component.scss'],
})
export class HorariosComponent implements OnInit {


  listaMedicamentos:any=[];

  constructor(
    private router: Router,
    private configuracionDataService: ConfiguracionDataService,
    private storage:Storage,
    public alertController: AlertController,
  ) { }

  ngOnInit() {
    this.configuracionDataService.mostrarMedicamentos();
    this.mostrarMedicamentos();

  }
   async ionViewWillEnter(){
    await this.configuracionDataService.mostrarMedicamentos();
    await this.mostrarMedicamentos();

  }

  addMedicamento(){
    this.router.navigate(['add-medicamento'])
  }

  async mostrarMedicamentos(){

    
    //await this.configuracionDataService.mostrarMedicamentos();
    this.listaMedicamentos=await this.storage.get('listaMedicacion');

    this.listaMedicamentos.sort(function(a, b) {
      return a.medicamento_franja_id - b.medicamento_franja_id;
    }); 


    console.log("HOLA MOSTRAR", this.listaMedicamentos)

    
  }

  async editarMedicamento(medicamento:any){

    this.router.navigate(['update-medicamento', medicamento]);
 

  }

  async borrarMedicamento(medicamento:any){

    console.log("BORRAR" ,medicamento)

      const alert = await this.alertController.create({
        cssClass: 'alert-class',
        header: 'Alerta',
        message: `¿Seguro que quieres borrar ${medicamento.medicamento_nombre} del horario?`,
        buttons: [
          {
            text: 'CANCELAR',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              //console.log('Confirm Cancel: blah');
              
            }
          }, {
            text: 'CONFIRMAR',
            handler: () => {
              console.log('Confirm Okay');
              this.borrar(medicamento);
            }
          }
        ]
      });
  
      await alert.present();
  }

  async borrar(medicacion){
    let borrado= await (await fetch(`${baseURL}medicacion/delete/${medicacion['id']}`, {
      method: "DELETE"
    })).json();
    console.log("BORRAR", medicacion)
     //this.ngOnInit();
    await this.configuracionDataService.mostrarMedicamentos();
    await this.mostrarMedicamentos();

  }


  async viewReports(){
    console.log("REPORTS")
    this.router.navigate(['reports']);
 
  }



}
