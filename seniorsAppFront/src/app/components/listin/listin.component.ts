import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfiguracionDataService } from 'src/app/services/configuracion-data.service';
import { Storage } from '@ionic/storage-angular';
import { AlertController } from '@ionic/angular';


const baseURL="https://unai-hernandez-7e3.alwaysdata.net/memory_senior/api/";
const options = {
  headers: {'Content-Type':'application/json'}
}

@Component({
  selector: 'app-listin',
  templateUrl: './listin.component.html',
  styleUrls: ['./listin.component.scss'],
})
export class ListinComponent implements OnInit {

  listin:any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private configuracionDataService: ConfiguracionDataService,
    private storage:Storage,
    public alertController: AlertController,
  ) { }

  ngOnInit() {

    this.configuracionDataService.mostrarTelefonosListin();
    this.mostrarTelefonosListin();

  }

  async mostrarTelefonosListin(){

    await this.configuracionDataService.mostrarTelefonosListin();

    this.listin=await this.storage.get('listaTelefonos');
  }


  addTelefono(){
    this.router.navigate(['add-numero-listin'])
  }



  editartelefono(numTel:any){

    this.router.navigate(['update-numero-listin', numTel]);

  }



  async borrartelefono(numTel:any){

      const alert = await this.alertController.create({
        cssClass: 'alert-class',
        header: 'Alerta',
        message: `¿Seguro que quieres borrar el número de telefono de ${numTel.numero_tel_nombre} del listin?`,
        buttons: [
          {
            text: 'CANCELAR',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
            
              
            }
          }, {
            text: 'CONFIRMAR',
            handler: () => {
              console.log('Confirm Okay');
              this.borrar(numTel);
            }
          }
        ]
      });

      await alert.present();
  }

    async borrar(numTel:any){
      let borrado= await (await fetch(`${baseURL}tel/delete/${numTel['id']}`, {
        method: "DELETE"
      })).json();
      console.log("BORRAR", numTel)
      await this.configuracionDataService.mostrarTelefonosListin();
      await this.mostrarTelefonosListin();

    }


}
