export interface Login {
    success: CuentaLogin;
}
export interface Register {
    success: CuentaRegister;
}

export interface CuentaLogin{
    id: number,
    cuenta_email: string,
    email_verified_at:string,
    created_at: string,
    updated_at:string
}

export interface CuentaRegister{    
    cuenta_email: string,
    updated_at:string,
    created_at: string,
    id: number,
}

export interface InfoCuenta{  
    id: number,  
    cuenta_email: string,
    nombreRes:string,
    nombreUse: string,
    
}

export interface DatosFormulario{  
    id: number,  
    nombreRes:string;
    cuenta_email:string;
    nombre:string;
    apellido:string;
    nacimiento:string;
    genero:string;
    email:string;
  
}

export interface ListaMedicamentos{
    status:string,
    data:[InfoMedicamento]
}

export interface InfoMedicamento{
    id: number,
    medicamento_horario_id:number,
    medicamento_franja_id:number,
    medicamento_nombre:string,
    medicamento_cantidad:string,
    created_at:string,
    updated_at:string



}
    