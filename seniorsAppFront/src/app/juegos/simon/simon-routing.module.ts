import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SimonPage } from './simon.page';

const routes: Routes = [
  {
    path: '',
    component: SimonPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SimonPageRoutingModule {}
