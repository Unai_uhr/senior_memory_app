import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
//import { format } from 'path';
import { ConfiguracionDataService } from 'src/app/services/configuracion-data.service';

@Component({
  selector: 'app-add-agenda',
  templateUrl: './add-agenda.page.html',
  styleUrls: ['./add-agenda.page.scss'],
})
export class AddAgendaPage implements OnInit {

  agendaItem={
    item_agenda_item:"",
    item_agenda_fecha:"",
    item_agenda_hora:"",
    item_agenda_descripcion:""

  }

  constructor(
    private router: Router,
    private configuracionDataService: ConfiguracionDataService,
  ) { }

  ngOnInit() {
  }

  addNewItemAgenda(){
    console.log("AGENDAITEM", this.agendaItem);

    let date: Date = new Date(this.agendaItem.item_agenda_hora);  
    let miHora=date.getHours()+":"+date.getMinutes()
    this.agendaItem.item_agenda_hora=miHora;

    this.configuracionDataService.addNewItemAgenda(this.agendaItem);
    this.configuracionDataService.mostrarItemsAgenda();
    const parametro={
      segmento:'agenda'
    }
    this.router.navigate(['loading',parametro]);
  }


  cancelAdd(){
    this.router.navigate(['configuracion'])
  }

}
