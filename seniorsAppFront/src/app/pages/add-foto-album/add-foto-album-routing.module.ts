import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddFotoAlbumPage } from './add-foto-album.page';

const routes: Routes = [
  {
    path: '',
    component: AddFotoAlbumPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddFotoAlbumPageRoutingModule {}
