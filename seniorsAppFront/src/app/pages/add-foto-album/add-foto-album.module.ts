import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddFotoAlbumPageRoutingModule } from './add-foto-album-routing.module';

import { AddFotoAlbumPage } from './add-foto-album.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddFotoAlbumPageRoutingModule
  ],
  declarations: [AddFotoAlbumPage]
})
export class AddFotoAlbumPageModule {}
