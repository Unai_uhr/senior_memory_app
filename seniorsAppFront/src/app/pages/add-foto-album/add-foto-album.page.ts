import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfiguracionDataService } from 'src/app/services/configuracion-data.service';
import { DataBaseFireBaseService } from 'src/app/services/data-base-fire-base.service';
import { Storage } from '@ionic/storage-angular';
import { AlertController } from '@ionic/angular';
import { AngularFireStorage ,AngularFireUploadTask} from '@angular/fire/storage';
import { finalize, ignoreElements, last, switchMap } from 'rxjs/operators'
import { Observable } from 'rxjs/internal/Observable';
import { concat, defer } from 'rxjs';






@Component({
  selector: 'app-add-foto-album',
  templateUrl: './add-foto-album.page.html',
  styleUrls: ['./add-foto-album.page.scss'],
})
export class AddFotoAlbumPage implements OnInit {

  idAlbum:number;

  comentarioKO:boolean;

  addFoto={
    foto_imagen:'',
    foto_comentario:'' 
  }

  uploadPercent :Observable<number>;
  downloadURL;
  urlImage : any;

  constructor(
    private router: Router,
    private databaseFireBaseService: DataBaseFireBaseService,
    private configuracionDataService: ConfiguracionDataService,
    private storage:Storage,
    private fireStorage: AngularFireStorage,
  ) { }

  ngOnInit() {

    this.storage.get('infoCuenta').then(async (result:any) => {

      this.idAlbum=await result.id_Album;

      })

    
  }


  cancelAdd(){
    this.router.navigate(['configuracion'])
  }

   cargarFoto(event){
    console.log("FOTO", event.target.files[0])

    const id = Math.random().toString(36).substring(2);
    const file =  event.target.files[0];
    const filePath = `senior_memory_app/album_${this.idAlbum}_${id}`;
    const ref = this.fireStorage.ref(filePath);
    const task= this.fireStorage.upload(filePath,file)

    this.uploadPercent = task.percentageChanges();
    this.urlImage = ref.getDownloadURL()

    task.snapshotChanges().pipe(
      last(),  
      switchMap(() => ref.getDownloadURL())
    ).subscribe(url => this.urlImage=url);


      
    /*task.snapshotChanges().pipe(
      finalize(async () => {
        this.urlImage =await ref.getDownloadURL(); // <-- Here the downloadURL is available.
        console.log(this.urlImage)
      })
    ).subscribe();*/
    
    /*task.snapshotChanges().pipe(
      finalize(() => {
          this.downloadURL = ref.getDownloadURL();
          console.log(this.downloadURL)
          this.downloadURL.subscribe(url=>{this.urlImage = url})
          console.log (this.urlImage)
      })
  )*/

    //task.snapshotChanges().pipe(finalize(() => this.urlImage = ref.getDownloadURL())).subscribe();

   console.log("URL ", this.urlImage)

  }

  addNewFoto(){

    console.log(this.addFoto.foto_comentario.length);

    

    if(this.addFoto.foto_comentario.length>0){

      this.comentarioKO=false

      console.log("URL", this.urlImage)

      const fotoInfo={
        foto_imagen:this.urlImage,
        foto_comentario:this.addFoto.foto_comentario
      }

      this.configuracionDataService.addNewFoto(fotoInfo);
      this.configuracionDataService.mostrarFotos();
      const parametro={
        segmento:'fotos'
      }
      this.router.navigate(['loading',parametro]);
    
    }else{
      this.comentarioKO=true;
    }

    

    //GUARDAR EN FIREBASE PARA OBTENER LA URL
    //let url= this.databaseFireBaseService.uploadImage(this.addFoto.foto_imagen)
    

    //GUARDAR EN BASE DE DATOS

    /*
    this.configuracionDataService.addNewFoto(this.addFoto);
    this.configuracionDataService.mostrarFotos();
    this.router.navigate(['configuracion']);
    */



  }

  

}
