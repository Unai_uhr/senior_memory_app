import { Component, OnInit } from '@angular/core';
import { Router, RouterModule } from '@angular/router';
import { ConfiguracionDataService } from 'src/app/services/configuracion-data.service';

@Component({
  selector: 'app-add-medicamento',
  templateUrl: './add-medicamento.page.html',
  styleUrls: ['./add-medicamento.page.scss'],
})
export class AddMedicamentoPage implements OnInit {

  franjas=[{
    id:'1',
    franja:'Desayuno'
  },{
    id:'2',
    franja:'Almuerzo'
  },{
    id:'3',
    franja:'Comida'
  },{
    id:'4',
    franja:'Merienda'
  },{
    id:'5',
    franja:'Cena'
  },{
    id:'6',
    franja:'Hora'
  },

]

  addMedicamento:any={
    franja:'',
    medicamento:'',
    cantidad:''
  }

  constructor(
    private router: Router,
    private configuracionDataService: ConfiguracionDataService,
  ) { }

  ngOnInit() {
  }

  cancelAdd(){
    this.router.navigate(['configuracion'])
  }

  addNewMedicamento(){
    this.configuracionDataService.addNewMedicamento(this.addMedicamento);
    this.configuracionDataService.mostrarMedicamentos();
    const parametro={
      segmento:'horarios'
    }
    this.router.navigate(['configuracion',parametro]);
  }

}
