import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddNumeroListinPage } from './add-numero-listin.page';

const routes: Routes = [
  {
    path: '',
    component: AddNumeroListinPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddNumeroListinPageRoutingModule {}
