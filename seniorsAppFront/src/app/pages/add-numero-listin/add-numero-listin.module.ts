import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddNumeroListinPageRoutingModule } from './add-numero-listin-routing.module';

import { AddNumeroListinPage } from './add-numero-listin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddNumeroListinPageRoutingModule
  ],
  declarations: [AddNumeroListinPage]
})
export class AddNumeroListinPageModule {}
