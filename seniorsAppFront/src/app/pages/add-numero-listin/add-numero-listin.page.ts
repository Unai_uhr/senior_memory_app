import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConfiguracionDataService } from 'src/app/services/configuracion-data.service';
import { DataBaseFireBaseService } from 'src/app/services/data-base-fire-base.service';
import { Storage } from '@ionic/storage-angular';
import { AlertController } from '@ionic/angular';
import { AngularFireStorage ,AngularFireUploadTask} from '@angular/fire/storage';
import { Observable } from 'rxjs/internal/Observable';
import { finalize, ignoreElements, last, switchMap } from 'rxjs/operators'

@Component({
  selector: 'app-add-numero-listin',
  templateUrl: './add-numero-listin.page.html',
  styleUrls: ['./add-numero-listin.page.scss'],
})
export class AddNumeroListinPage implements OnInit {

  idListin:number;

  addTelefono={
    numero_tel_imagen:"",
    numero_tel_nombre:"",
    numero_tel_numero:""

  }

  uploadPercent :Observable<number>;
  downloadURL;
  urlImage : any;

  constructor(

    private router: Router,
    private databaseFireBaseService: DataBaseFireBaseService,
    private configuracionDataService: ConfiguracionDataService,
    private storage:Storage,
    private fireStorage: AngularFireStorage,
    
  ) { }

  ngOnInit() {
    this.storage.get('infoCuenta').then(async (result:any) => {

      this.idListin=await result.id_Listin;

      })
  }

  cancelAdd(){
    this.router.navigate(['configuracion'])
  }

  cargarFoto(event){
    console.log("FOTO", event.target.files[0])

    const id = Math.random().toString(36).substring(2);
    const file =  event.target.files[0];
    const filePath = `senior_memory_app/album_${this.idListin}_${id}`;
    const ref = this.fireStorage.ref(filePath);
    const task= this.fireStorage.upload(filePath,file)

    this.uploadPercent = task.percentageChanges();
    this.urlImage = ref.getDownloadURL()

    task.snapshotChanges().pipe(
      last(),  
      switchMap(() => ref.getDownloadURL())
    ).subscribe(url => this.urlImage=url);

  }

  addNewTelefono(){

    if(this.addTelefono.numero_tel_nombre.length>0 && this.addTelefono.numero_tel_numero.length>0){

      this.addTelefono.numero_tel_imagen=this.urlImage;

      //GUARDAR DATOS EN BASEDATOS
      this.configuracionDataService.addNewTel(this.addTelefono);
      this.configuracionDataService.mostrarTelefonosListin();
      const parametro={
        segmento:'telefonos'
      }
      this.router.navigate(['configuracion',parametro]);

    }else{
      //Error
    }


    

  }

}
