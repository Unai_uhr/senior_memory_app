import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfiguracionDataService } from 'src/app/services/configuracion-data.service';


@Component({
  selector: 'app-configuracion',
  templateUrl: './configuracion.page.html',
  styleUrls: ['./configuracion.page.scss'],
})
export class ConfiguracionPage implements OnInit {

  segmentoActual:string;
  datos:boolean=true;
  datosRegister:any;

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private configuracionDataService: ConfiguracionDataService,

  ) { 
  
    this.segmentoActual = 'datosPersonales';
  }

  ngOnInit() {

    console.log("ngOnInit",this.segmentoActual)
    
    /*if(this.segmentoActual==undefined){
      this.segmentoActual = 'datosPersonales';
    }*/

    this.route.params.subscribe( async params => {
      this.segmentoActual=await params.segmento;
      
      });
    
    
  }

  ionViewWillEnter(){
    console.log("ESTOY EN CONFIGURACION WILLENTER")
    this.configuracionDataService.mostrarFotos();
    this.configuracionDataService.mostrarMedicamentos();
    this.configuracionDataService.mostrarItemsAgenda();
    this.configuracionDataService.mostrarTelefonosListin();

    this.route.params.subscribe( async params => {
      this.segmentoActual=await params.segmento;
      
      });
      /*if(this.segmentoActual==undefined){
        this.segmentoActual = 'datosPersonales';
      }*/
      console.log(this.segmentoActual)

  }

  segmentChanged(event){
    //console.log(event.detail.value);
    this.datos = false;

  }
 

}
