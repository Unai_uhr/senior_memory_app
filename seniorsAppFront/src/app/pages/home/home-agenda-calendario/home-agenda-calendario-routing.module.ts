import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAgendaCalendarioPage } from './home-agenda-calendario.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAgendaCalendarioPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAgendaCalendarioPageRoutingModule {}
