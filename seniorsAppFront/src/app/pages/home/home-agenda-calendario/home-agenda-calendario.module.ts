import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAgendaCalendarioPageRoutingModule } from './home-agenda-calendario-routing.module';

import { HomeAgendaCalendarioPage } from './home-agenda-calendario.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAgendaCalendarioPageRoutingModule,
    ComponentsModule
  ],
  declarations: [HomeAgendaCalendarioPage]
})
export class HomeAgendaCalendarioPageModule {}
