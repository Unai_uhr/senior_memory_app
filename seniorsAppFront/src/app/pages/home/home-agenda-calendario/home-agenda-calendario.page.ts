import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';


const baseURL="https://unai-hernandez-7e3.alwaysdata.net/memory_senior/api/";
const options = {
  headers: {'Content-Type':'application/json'}
}

@Component({
  selector: 'app-home-agenda-calendario',
  templateUrl: './home-agenda-calendario.page.html',
  styleUrls: ['./home-agenda-calendario.page.scss'],
})
export class HomeAgendaCalendarioPage implements OnInit {

  
  
    nombre:string="";
    agenda:any;
    agendaHoy:any;
    agendaManana:any;
    agendaResto:any;

  constructor(
    private storage:Storage,
    private route: ActivatedRoute, 
    private router: Router,
  ) { 

    this.storage.get('infoCuenta').then(async (result) => {
      this.nombre=await result.usuario_nombre;
    });

  }

  ngOnInit() {

    this.storage.get('listaAgenda').then(async (result) => {
      this.agenda=await result;
      this.filtrarEventos();
    });

  }

  async filtrarEventos(){

    //busco los eventos del dia de hoy
    let todayDate= new Date;
    let today=todayDate.toDateString();

    
    this.storage.get('infoCuenta').then(async (result) => {
      let id_Agenda=await result.id_Agenda;
      this.agendaHoy=await(await fetch(`${baseURL}agenda-item/all-fecha/${id_Agenda}/${today}`, {
        method: "GET"
      })).json();
      this.agendaHoy= await this.agendaHoy.data;

    });

    //busco los eventos de mañana
    let tomorrowDate =new Date(new Date(). getTime() + 24 * 60 * 60 * 1000);
    let tomorrow =tomorrowDate.toDateString();


    this.storage.get('infoCuenta').then(async (result) => {
      let id_Agenda=await result.id_Agenda;
      this.agendaManana=await(await fetch(`${baseURL}agenda-item/all-fecha/${id_Agenda}/${tomorrow}`, {
        method: "GET"
      })).json();
      this.agendaManana= await this.agendaManana.data;
   
    });
    //busco los eventos de a partir de pasada mañana
    let pasadoMananaDate =new Date(new Date(tomorrowDate). getTime() + 24 * 60 * 60 * 1000);
    let pasadoManana =pasadoMananaDate.toDateString();

    this.storage.get('infoCuenta').then(async (result) => {
      let id_Agenda=await result.id_Agenda;
      this.agendaResto= await(await fetch(`${baseURL}agenda-item/all/${id_Agenda}/${pasadoManana}`, {
        method: "GET"
      })).json();
      this.agendaResto= await this.agendaResto.data;
   
    });



  }
  


}
