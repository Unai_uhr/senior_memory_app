import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FotoDetallePage } from './foto-detalle.page';

const routes: Routes = [
  {
    path: '',
    component: FotoDetallePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FotoDetallePageRoutingModule {}
