import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FotoDetallePageRoutingModule } from './foto-detalle-routing.module';

import { FotoDetallePage } from './foto-detalle.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FotoDetallePageRoutingModule
  ],
  declarations: [FotoDetallePage]
})
export class FotoDetallePageModule {}
