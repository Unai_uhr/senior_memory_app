import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-foto-detalle',
  templateUrl: './foto-detalle.page.html',
  styleUrls: ['./foto-detalle.page.scss'],
})
export class FotoDetallePage implements OnInit {

  foto:any;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public navCtrl:NavController
  ) { 
    

      this.route.params.subscribe( params => {
        this.foto= params
        console.log("FOTO",this.foto)
        });
  
  

  }

  ngOnInit() {
  }


  async goBack(){
    console.log("GOBACK")
    //this.navCtrl.pop();
    await this.router.navigate(['/home/home-album'])
  }

}
