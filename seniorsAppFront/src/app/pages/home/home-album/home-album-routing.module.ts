import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeAlbumPage } from './home-album.page';

const routes: Routes = [
  {
    path: '',
    component: HomeAlbumPage
  },
  {
    path: 'foto-detalle',
    loadChildren: () => import('./foto-detalle/foto-detalle.module').then( m => m.FotoDetallePageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeAlbumPageRoutingModule {}
