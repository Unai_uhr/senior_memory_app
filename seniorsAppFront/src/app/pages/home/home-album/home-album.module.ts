import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeAlbumPageRoutingModule } from './home-album-routing.module';

import { HomeAlbumPage } from './home-album.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeAlbumPageRoutingModule,
    ComponentsModule
  ],
  declarations: [HomeAlbumPage]
})
export class HomeAlbumPageModule {}
