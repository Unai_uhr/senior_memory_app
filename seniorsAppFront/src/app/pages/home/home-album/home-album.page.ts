import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { ModalController } from '@ionic/angular';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-home-album',
  templateUrl: './home-album.page.html',
  styleUrls: ['./home-album.page.scss'],
})
export class HomeAlbumPage implements OnInit {

  albumFotos:any;
  nombre:string;

  constructor(
    private storage:Storage,
    private route: ActivatedRoute, 
    private router: Router,
  ) {

    this.storage.get('infoCuenta').then(async (result) => {
      this.nombre=await result.usuario_nombre;
    });

   }

  ngOnInit() {

    this.storage.get('listaFotos').then(async (result) => {
      this.albumFotos=await result;
      console.log(this.nombre)
    });

    
  }


   verFoto(foto) {
    console.log(foto)
    this.router.navigate(['home/home-album/foto-detalle', foto]);
   
  }



}
