import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeListinPage } from './home-listin.page';

const routes: Routes = [
  {
    path: '',
    component: HomeListinPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeListinPageRoutingModule {}
