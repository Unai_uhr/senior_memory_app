import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeListinPageRoutingModule } from './home-listin-routing.module';

import { HomeListinPage } from './home-listin.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeListinPageRoutingModule,
    ComponentsModule
  ],
  declarations: [HomeListinPage]
})
export class HomeListinPageModule {}
