import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';

@Component({
  selector: 'app-home-listin',
  templateUrl: './home-listin.page.html',
  styleUrls: ['./home-listin.page.scss'],
})
export class HomeListinPage implements OnInit {

  telefonos:any;
  nombre:string;

  constructor(

    private storage:Storage,
    private route: ActivatedRoute, 
    private router: Router,

  ) { 
    this.storage.get('infoCuenta').then(async (result) => {
      this.nombre=await result.usuario_nombre;
    });
  }

  ngOnInit() {

    this.storage.get('listaTelefonos').then(async (result) => {
      this.telefonos=await result;
      console.log(this.nombre)
    });

  }

}
