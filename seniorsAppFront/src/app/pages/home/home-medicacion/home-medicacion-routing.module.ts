import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeMedicacionPage } from './home-medicacion.page';

const routes: Routes = [
  {
    path: '',
    component: HomeMedicacionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeMedicacionPageRoutingModule {}
