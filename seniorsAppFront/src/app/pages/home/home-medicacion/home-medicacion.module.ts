import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeMedicacionPageRoutingModule } from './home-medicacion-routing.module';

import { HomeMedicacionPage } from './home-medicacion.page';
import { ComponentsModule } from 'src/app/components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeMedicacionPageRoutingModule,
    ComponentsModule
  ],
  declarations: [HomeMedicacionPage]
})
export class HomeMedicacionPageModule {}
