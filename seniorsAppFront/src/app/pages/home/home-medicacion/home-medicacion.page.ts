import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';


const baseURL="https://unai-hernandez-7e3.alwaysdata.net/memory_senior/api/";

const options = {
  headers: {'Content-Type':'application/json'}
}

@Component({
  selector: 'app-home-medicacion',
  templateUrl: './home-medicacion.page.html',
  styleUrls: ['./home-medicacion.page.scss'],
})
export class HomeMedicacionPage implements OnInit {

  nombre:string="";
  id_control:number;
  medicamentos:any;
  medicamentosHoy:any;
  listaDeChecked:any;
  horario_id:any;

  constructor(
    private storage:Storage,
    private route: ActivatedRoute, 
    private router: Router,
  ) { }

  ngOnInit() {

    let todayDate= new Date;
    let today;
    if(todayDate.getMonth()<=8){
       today=todayDate.getFullYear()+"-0"+(todayDate.getMonth()+1)+"-"+todayDate.getDate()
    }else{
      today=todayDate.getFullYear()+"-"+(todayDate.getMonth()+1)+"-"+todayDate.getDate()
    }
    

    this.storage.get('infoCuenta').then(async (result:any) => {
      this.nombre=await result.usuario_nombre;
      this.id_control=await result.id_Control;
      this.horario_id=await result.id_Horario;

     this.listaDeChecked= await (await fetch(`${baseURL}control-medicacion/get-all/${this.horario_id}`,{
        method:"GET"
      })).json();

      this.medicamentos=await this.listaDeChecked[0].filter(medic => medic.fecha === today);


      this.medicamentos.sort(function(a, b) {
        return a.medicamento_franja - b.medicamento_franja;
      }); 

   
      console.log("DEL DIA DE HOY", this.medicamentos)


    });

    /*this.storage.get('listaMedicacion').then(async (result) => {
      this.medicamentos=await result;
    });*/
    console.log("MEDICAMENTOS", this.medicamentos)




  }

  guardarMedicamentoTomado(medicina,$event){
    console.log($event['detail'].checked);
    

    let medicacionValidada:any;

    this.storage.get('infoCuenta').then(async (result:any) => {
      this.id_control=await result.id_Control;

      medicacionValidada= await (await fetch(`${baseURL}items-control-medicacion/update-item/${this.id_control}/${medicina.medicamento_id}/${$event['detail'].checked}`,{
        method:"PUT"
      })).json();

    });

    //GUARDAR medicamento tomado
  }

}
