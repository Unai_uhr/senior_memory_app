import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
  },
  {
    path: 'home-album',
    loadChildren: () => import('./home-album/home-album.module').then( m => m.HomeAlbumPageModule)
  },
  {
    path: 'home-listin',
    loadChildren: () => import('./home-listin/home-listin.module').then( m => m.HomeListinPageModule)
  },
  {
    path: 'home-juegos',
    loadChildren: () => import('./home-juegos/home-juegos.module').then( m => m.HomeJuegosPageModule)
  },
  {
    path: 'home-medicacion',
    loadChildren: () => import('./home-medicacion/home-medicacion.module').then( m => m.HomeMedicacionPageModule)
  },
  {
    path: 'home-agenda-calendario',
    loadChildren: () => import('./home-agenda-calendario/home-agenda-calendario.module').then( m => m.HomeAgendaCalendarioPageModule)
  },
  {
    path: 'hola/home-album',
    loadChildren: () => import('./home-album/home-album.module').then( m => m.HomeAlbumPageModule)
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
