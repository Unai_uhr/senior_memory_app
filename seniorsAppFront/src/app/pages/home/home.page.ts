import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { ConfiguracionDataService } from 'src/app/services/configuracion-data.service';
//import { Vibration } from '@ionic-native/vibration/ngx';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(
   //private vibration: Vibration,
    private router: Router,
    private configuracionDataService: ConfiguracionDataService,

  ) {

    this.configuracionDataService.mostrarFotos();
    this.configuracionDataService.mostrarMedicamentos();
    this.configuracionDataService.mostrarItemsAgenda();
    this.configuracionDataService.mostrarTelefonosListin();


  }


  homeAlbum(){
    //this.vibration.vibrate(500);
    console.log('album')
    this.router.navigate(['home/home-album'])
  }

  homeAgendaCalendario(){
    this.router.navigate(['home/home-agenda-calendario'])
  }

  homeListin(){
    this.router.navigate(['home/home-listin'])
  }
  homeJuegos(){
    this.router.navigate(['simon'])
  }
  homeMedicacion(){
    this.router.navigate(['home/home-medicacion'])
  }

  

}
