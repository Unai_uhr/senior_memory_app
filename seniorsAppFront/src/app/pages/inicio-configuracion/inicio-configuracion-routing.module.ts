import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InicioConfiguracionPage } from './inicio-configuracion.page';

const routes: Routes = [
  {
    path: '',
    component: InicioConfiguracionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InicioConfiguracionPageRoutingModule {}
