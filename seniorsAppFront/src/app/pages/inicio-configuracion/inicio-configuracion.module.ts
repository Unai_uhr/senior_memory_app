import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InicioConfiguracionPageRoutingModule } from './inicio-configuracion-routing.module';

import { InicioConfiguracionPage } from './inicio-configuracion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InicioConfiguracionPageRoutingModule
  ],
  declarations: [InicioConfiguracionPage]
})
export class InicioConfiguracionPageModule {}
