import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-inicio-configuracion',
  templateUrl: './inicio-configuracion.page.html',
  styleUrls: ['./inicio-configuracion.page.scss'],
})
export class InicioConfiguracionPage implements OnInit {

  constructor(

    private router : Router
  ) { }

  ngOnInit() {
  }

  irConfiguracion(){
    const parametro={
      segmento:'datosPersonales'
    }
    this.router.navigate(['loading',parametro]);
  }

}
