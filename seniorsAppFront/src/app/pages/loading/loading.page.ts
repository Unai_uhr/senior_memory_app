import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras } from '@angular/router';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';





@Component({
  selector: 'app-loading',
  templateUrl: './loading.page.html',
  styleUrls: ['./loading.page.scss'],
})
export class LoadingPage implements OnInit {


  parametro:any={
    
  }
  

  constructor(
    public router: Router,
    private route: ActivatedRoute, 
    public navCtrl: NavController
    
  ) { 
    this.route.params.subscribe( async params => {
      this.parametro=await params;
      console.log("params",params.segmento )
      });

  }

  ngOnInit() {

    setInterval(()=>this.router.navigate(['configuracion',this.parametro]),1500)

  }

  navegar(){
    this.router.navigate(['configuracion',this.parametro])
  }

 


}
