import { Component, OnInit } from '@angular/core';
import { LoginDataService } from '../../services/login-data.service';
import { NavController } from '@ionic/angular';
import { Storage } from '@ionic/storage-angular';
import { Router } from '@angular/router';


const baseURL="https://unai-hernandez-7e3.alwaysdata.net/memory_senior/api/";

const options = {
  headers: {'Content-Type':'application/json'}
}


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  cuenta_email:string;
  password:string;

  constructor(
   public loginData : LoginDataService, 
   private navController:NavController,
   private storage:Storage,
   private router: Router,
  ) { }

  ngOnInit() {

    //Si hay sesión iniciada salta el login
    this.storage.get('infoCuenta').then(async (result) => {
      if(result!=null){


        //comprobamos si hay control para hoy
        let todayDate= new Date;
        let today=todayDate.toDateString();
        let idHorario= result.id_Horario;

        let controlHoyHay = await (await fetch(`${baseURL}control-medicacion/get-one/${idHorario}/${today}`,{
          method:"GET"
        })).json();
        let controlHoy;
        let id_ControlHoyId:number;
        
        //SI no existe el de hoy se crea y se rellena en false
        if("error" in controlHoyHay){
          console.log("CONTROL ERROR", "error" in controlHoyHay);
          controlHoy = await (await fetch(`${baseURL}control-medicacion/create/${idHorario}/${today}`,{
            method:"POST"
          })).json();
          console.log("CONTROL", controlHoy);
          id_ControlHoyId=controlHoy.id;
          //Llenamos el control en false
          
          fetch(`${baseURL}items-control-medicacion/insert-all/${id_ControlHoyId}/${idHorario}`,{
            method:"POST"
          })

        //Si existe el de hoy se busca el id sin mas
        }else if('status' in controlHoyHay){
          console.log("CONTROL OK", "status" in controlHoyHay);
          id_ControlHoyId=controlHoyHay['data'].id;
        }






        this.router.navigate(['home'])
      }

    });
    


  }

   login(){
  
     this.loginData.login(this.cuenta_email, this.password);
  
   

    
      
  }

  registrarse(){
    this.navController.navigateForward('register');
  }

}
