import { Component, OnInit } from '@angular/core';
import { LoginDataService } from 'src/app/services/login-data.service';
import { NavController } from '@ionic/angular';
import { Login, Register } from './../../interfaces/interfaces';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  responsable_nombre:string="";
  koResponsable_nombre:boolean;
  cuenta_email:string="";;
  koCuenta_email:boolean;
  password:string="";;
  koPassword:boolean;
  passwordBis:string="";;
  koPasswordBis:boolean;
  myResponse:any;

  constructor(
    public loginData : LoginDataService, 
    private navController:NavController,
    private router : Router
  ) { }

  ngOnInit() {
  }

  async createAcount(){
      let acount=this.myResponse=await this.loginData.createAcount(this.cuenta_email, this.password ,this.responsable_nombre);
      /*console.log("VIENE DEL SERVICE=> ", hola)
      let navigationExtras :NavigationExtras ={
       queryParams:{
        cuenta_id : acount.id,
        responsable_nombre: this.responsable_nombre,
        cuenta_email :this.cuenta_email,
        
       }
     }*/
     this.router.navigate(['inicio-configuracion'])
      
     
  }

  checkInput(campo:string){
    switch (campo){
      case 'name':
        if(this.responsable_nombre.length==0){
          this.koResponsable_nombre=true;
        }else{
          this.koResponsable_nombre=false;
        }
        break;
      case 'email' :
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        if(!re.test(this.cuenta_email)) {
          this.koCuenta_email=true;
        }else{
          this.koCuenta_email=false;
        }
        break;
      case 'pass':
        var passPatern=/^(?=\w*\d)(?=\w*[A-Z])(?=\w*[a-z])\S{8,16}$/;
        if(!passPatern.test(this.password)) {
          this.koPassword=true;
          if(this.password!==this.passwordBis) {
            this.koPasswordBis=true;
          }else{
            this.koPasswordBis=false;
          }
        }else{
          this.koPassword=false;
          if(this.password!==this.passwordBis) {
            this.koPasswordBis=true;
          }else{
            this.koPasswordBis=false;
          }
        }
        break;
      case 'passBis' :
        if(this.password!==this.passwordBis) {
          this.koPasswordBis=true;
        }else{
          this.koPasswordBis=false;
        }
        break;
      case 'final' :
        if(!this.koResponsable_nombre && !this.koCuenta_email && !this.koPassword && !this.koPasswordBis) {
          this.createAcount()
        }
        break;
    }
  }

}
var hola;
