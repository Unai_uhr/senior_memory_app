import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage-angular';


const baseURL="https://unai-hernandez-7e3.alwaysdata.net/memory_senior/api/";

const options = {
  headers: {'Content-Type':'application/json'}
}

@Component({
  selector: 'app-reports',
  templateUrl: './reports.page.html',
  styleUrls: ['./reports.page.scss'],
})
export class ReportsPage implements OnInit {

  horarioId:number;
  infoReports:any;
  infoReportsFilteded:any;
  controlesIDs:any=[];


  constructor(
    private router: Router,
    private storage:Storage,
  ) { }

  ngOnInit() {

    this.storage.get('infoCuenta').then(async (result) => {
      this.horarioId=await result.id_Horario;

      this.cargarControles();
    });

    

  }

  async cargarControles(){
    this.infoReports=await (await fetch(`${baseURL}control-medicacion/get-all/${this.horarioId}`, {
      method: "GET"
    })).json();

    let idIni=await this.infoReports[0][0].control_id;
    this.controlesIDs.push(this.infoReports[0][0].control_id);
    this.controlesIDs=this.infoReports[0].filter((item:any) => {

      if(item.control_id!=idIni){
        this.controlesIDs.push(item.control_id);
        idIni=item.control_id;
      }
      
    });

    this.infoReports=this.infoReports[0].reverse()
    

    

    console.log(this.infoReports)
  }

  

  goHome(){
    const parametro={
      segmento:'horarios'
    }
    this.router.navigate(['configuracion',parametro]);
  
  }

}
