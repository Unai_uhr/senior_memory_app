import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateAgendaPage } from './update-agenda.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateAgendaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateAgendaPageRoutingModule {}
