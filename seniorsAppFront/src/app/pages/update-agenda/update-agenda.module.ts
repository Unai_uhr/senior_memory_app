import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateAgendaPageRoutingModule } from './update-agenda-routing.module';

import { UpdateAgendaPage } from './update-agenda.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateAgendaPageRoutingModule
  ],
  declarations: [UpdateAgendaPage]
})
export class UpdateAgendaPageModule {}
