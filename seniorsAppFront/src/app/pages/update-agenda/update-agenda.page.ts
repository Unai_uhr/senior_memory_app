import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfiguracionDataService } from 'src/app/services/configuracion-data.service';

const baseURL="https://unai-hernandez-7e3.alwaysdata.net/memory_senior/api/";

@Component({
  selector: 'app-update-agenda',
  templateUrl: './update-agenda.page.html',
  styleUrls: ['./update-agenda.page.scss'],
})
export class UpdateAgendaPage implements OnInit {

  agendaItem={
    id:0,
    item_agenda_agenda_id:0,
    item_agenda_item:"",
    item_agenda_fecha:"",
    item_agenda_hora:"",
    item_agenda_descripcion:""

  }

  constructor(

    private router: Router,
    private configuracionDataService: ConfiguracionDataService,
    private route: ActivatedRoute,
    private http: HttpClient,
  ) {

    this.cargarDatos();
   }

  ngOnInit() {
  }


  async cargarDatos(){

    this.route.params.subscribe( async params => {
      this.agendaItem.id = params.id;
      this.agendaItem.item_agenda_item = params.item_agenda_item;
      this.agendaItem.item_agenda_fecha = params.item_agenda_fecha;
      this.agendaItem.item_agenda_hora = params.item_agenda_hora;
      this.agendaItem.item_agenda_descripcion = params.item_agenda_descripcion;
      
      });

      //console.log(this.agendaItem)
  }


  cancelAdd(){
    this.router.navigate(['configuracion'])
  }

  updateItemAgenda(){

    let itemToUpdate={
      item_agenda_item:this.agendaItem.item_agenda_item,
      item_agenda_fecha:this.agendaItem.item_agenda_fecha,
      item_agenda_hora:this.agendaItem.item_agenda_hora,
      item_agenda_descripcion:this.agendaItem.item_agenda_descripcion
    }


    fetch(`${baseURL}agenda-item/update/${this.agendaItem.id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(itemToUpdate)
      
    })
    .then(response => {
      console.log(response);
      this.router.navigate(['configuracion'])
    })
    .catch(err => {
      console.error(err);
    });

  }

}
