import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateFotoAlbumPage } from './update-foto-album.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateFotoAlbumPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateFotoAlbumPageRoutingModule {}
