import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateFotoAlbumPageRoutingModule } from './update-foto-album-routing.module';

import { UpdateFotoAlbumPage } from './update-foto-album.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateFotoAlbumPageRoutingModule
  ],
  declarations: [UpdateFotoAlbumPage]
})
export class UpdateFotoAlbumPageModule {}
