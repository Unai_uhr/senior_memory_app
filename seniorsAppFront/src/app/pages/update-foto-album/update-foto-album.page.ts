import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfiguracionDataService } from 'src/app/services/configuracion-data.service';

const baseURL="https://unai-hernandez-7e3.alwaysdata.net/memory_senior/api/";

@Component({
  selector: 'app-update-foto-album',
  templateUrl: './update-foto-album.page.html',
  styleUrls: ['./update-foto-album.page.scss'],
})
export class UpdateFotoAlbumPage implements OnInit {

  fotografia={
    id:0,
    foto_album_foto_id:0,
    foto_imagen:"",
    foto_comentario:""
  }

  constructor(

    private router: Router,
    private configuracionDataService: ConfiguracionDataService,
    private route: ActivatedRoute,
    private http: HttpClient,

  ) { }

  ngOnInit() {

    this.cargarDatos();
  }

  cancelAdd(){
    this.router.navigate(['configuracion'])
  }

  async cargarDatos(){
    this.route.params.subscribe( async params => {
      this.fotografia.id = params.id;
      this.fotografia.foto_album_foto_id = params.foto_album_foto_id;
      this.fotografia.foto_imagen = params.foto_imagen;
      this.fotografia.foto_comentario = params.foto_comentario;

      });

  }

  updateFoto(){


    fetch(`${baseURL}fotos/update/${this.fotografia.id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(this.fotografia)
      
    })
    .then(response => {
      console.log(response);
      this.router.navigate(['configuracion'])
    })
    .catch(err => {
      console.error(err);
    });

  }

}
