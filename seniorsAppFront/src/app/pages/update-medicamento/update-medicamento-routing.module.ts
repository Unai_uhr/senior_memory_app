import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateMedicamentoPage } from './update-medicamento.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateMedicamentoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateMedicamentoPageRoutingModule {}
