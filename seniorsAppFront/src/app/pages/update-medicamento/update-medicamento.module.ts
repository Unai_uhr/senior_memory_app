import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateMedicamentoPageRoutingModule } from './update-medicamento-routing.module';

import { UpdateMedicamentoPage } from './update-medicamento.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateMedicamentoPageRoutingModule
  ],
  declarations: [UpdateMedicamentoPage]
})
export class UpdateMedicamentoPageModule {}
