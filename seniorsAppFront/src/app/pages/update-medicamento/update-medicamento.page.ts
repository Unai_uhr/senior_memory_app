import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ConfiguracionDataService } from 'src/app/services/configuracion-data.service';

const baseURL="https://unai-hernandez-7e3.alwaysdata.net/memory_senior/api/";

@Component({
  selector: 'app-update-medicamento',
  templateUrl: './update-medicamento.page.html',
  styleUrls: ['./update-medicamento.page.scss'],
})
export class UpdateMedicamentoPage implements OnInit {
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  franjas=[{
    id:'1',
    franja:'Desayuno'
  },{
    id:'2',
    franja:'Almuerzo'
  },{
    id:'3',
    franja:'Comida'
  },{
    id:'4',
    franja:'Merienda'
  },{
    id:'5',
    franja:'Cena'
  },{
    id:'6',
    franja:'Hora'
  }]

  updateMedicamento:any={
    id:0,
    franja:'',
    medicamento:'',
    cantidad:''
  }

  constructor(
    private router: Router,
    private configuracionDataService: ConfiguracionDataService,
    private route: ActivatedRoute,
    private http: HttpClient,
    ) { 
      this.cargarDatos();

    }

  ngOnInit() {

    
    
  }
 ionViewWillEnter(){

  }

  async cargarDatos(){

     this.route.params.subscribe( async params => {
      this.updateMedicamento.id = params.id;
      this.updateMedicamento.franja = params.medicamento_franja_id;
      this.updateMedicamento.medicamento = params.medicamento_nombre;
      this.updateMedicamento.cantidad = params.medicamento_cantidad;
      
      });
      console.log(this.updateMedicamento)

      
 
  }

  cancelAdd(){
    this.router.navigate(['configuracion'])
  }

  toUpdateMedicamento(){
    console.log("medicamento",this.updateMedicamento)

    let toUpdate={
      medicamento_franja_id:Number( this.updateMedicamento.franja),
      medicamento_nombre:this.updateMedicamento.medicamento,
      medicamento_cantidad:this.updateMedicamento.cantidad
    }
    console.log("medicamentoII",toUpdate)

    fetch(`${baseURL}medicacion/update/${this.updateMedicamento.id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json"
        },
        body: JSON.stringify(toUpdate)
        
      })
      .then(response => {
        console.log(response);
        this.router.navigate(['configuracion'])
      })
      .catch(err => {
        console.error(err);
      });

  }

}
