import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateNumeroListinPage } from './update-numero-listin.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateNumeroListinPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateNumeroListinPageRoutingModule {}
