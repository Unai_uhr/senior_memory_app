import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateNumeroListinPageRoutingModule } from './update-numero-listin-routing.module';

import { UpdateNumeroListinPage } from './update-numero-listin.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateNumeroListinPageRoutingModule
  ],
  declarations: [UpdateNumeroListinPage]
})
export class UpdateNumeroListinPageModule {}
