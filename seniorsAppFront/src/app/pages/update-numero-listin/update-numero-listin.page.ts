import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfiguracionDataService } from 'src/app/services/configuracion-data.service';


const baseURL="https://unai-hernandez-7e3.alwaysdata.net/memory_senior/api/";

@Component({
  selector: 'app-update-numero-listin',
  templateUrl: './update-numero-listin.page.html',
  styleUrls: ['./update-numero-listin.page.scss'],
})
export class UpdateNumeroListinPage implements OnInit {

  toUpdateTelefono={
    id:0,
    numero_tel_listin_id:0,
    numero_tel_imagen:"",
    numero_tel_nombre:"",
    numero_tel_numero:"",
  }

  constructor(

    private router: Router,
    private configuracionDataService: ConfiguracionDataService,
    private route: ActivatedRoute,
    private http: HttpClient,

  ) { }

  ngOnInit() {

    this.cargarDatos();

  }

  cancelAdd(){
    this.router.navigate(['configuracion'])
  }

  async cargarDatos(){
    this.route.params.subscribe( async params => {
      this.toUpdateTelefono.id = params.id;
      this.toUpdateTelefono.numero_tel_listin_id = params.numero_tel_listin_id;
      this.toUpdateTelefono.numero_tel_nombre = params.numero_tel_nombre;
      this.toUpdateTelefono.numero_tel_numero = params.numero_tel_numero;

      });

  }

  updateTelefono(){

    //UPDATE FOTO DE FIREBASE

    console.log(this.toUpdateTelefono)

    fetch(`${baseURL}tel/update/${this.toUpdateTelefono.id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(this.toUpdateTelefono)
      
    })
    .then(response => {
      console.log(response);
      this.router.navigate(['configuracion'])
    })
    .catch(err => {
      console.error(err);
    });



  }

  




}
