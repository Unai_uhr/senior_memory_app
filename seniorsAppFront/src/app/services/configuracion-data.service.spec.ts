import { TestBed } from '@angular/core/testing';

import { ConfiguracionDataService } from './configuracion-data.service';

describe('ConfiguracionDataService', () => {
  let service: ConfiguracionDataService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConfiguracionDataService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
