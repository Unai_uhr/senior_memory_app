import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { Login, Register ,InfoCuenta, DatosFormulario ,ListaMedicamentos} from '../interfaces/interfaces';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NavigationExtras } from '@angular/router';




const baseURL="https://unai-hernandez-7e3.alwaysdata.net/memory_senior/api/";
const options = {
  headers: {'Content-Type':'application/json'}
}

@Injectable({
  providedIn: 'root'
})
export class ConfiguracionDataService {

  id_Horario:number;
  id_Agenda: number;
  id_Album:number;
  id_Listin:number;

  constructor(

    private storage:Storage,
    private http: HttpClient
  ) { 

    this.storage.create();
  }


  async guardarDatosPersonales(datosFormulario:any){
    var dateString = datosFormulario.nacimiento;

    var dateObject = new Date(dateString);

    let jsonUsuario={
      usuario_nombre:datosFormulario.nombre,
      usuario_apellidos:datosFormulario.apellido,
      usuario_fecha_nacimiento:dateObject,
      usuario_genero: "x",
      usuario_email: datosFormulario.email
    }

    console.log("DATOS FORMULARIO",datosFormulario)
    //Insertamos el usuario
    let restUsuario=await this.http.post(`${baseURL}usuarios/create/${datosFormulario.idCuenta}` , JSON.stringify(jsonUsuario),options).subscribe((json: any)=> 
    {
        console.log(json)
    },(error)=>{
      
      console.log(error)
    });

    //Insertamos al usuario responsable
    let restResponsable= await (await fetch(`${baseURL}responsables/create/${datosFormulario.idCuenta}`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: `{\"responsable_nombre\":\"${datosFormulario.nombreRes}\"} `
    })).json();

    this.storage.get('infoCuenta').then((result) => {
      console.log('My result', result);

      result.nombre=datosFormulario.nombre;

      console.log('My result LUEGO', result);
      this.storage.set('infoCuenta', result);
    });

    if(restUsuario && restResponsable){
      return "success";
    }
      
    }


    async addNewMedicamento(newMedicamento:any){

      let medicina:any={
        medicamento_franja_id:newMedicamento.franja,
        medicamento_nombre:newMedicamento.medicamento,
        medicamento_cantidad:newMedicamento.cantidad

      }
      //let listaMedicamentos= await this.storage.get('listaMedicacion')

      console.log("NEW",newMedicamento)

      this.storage.get('infoCuenta').then(async (result) => {
        this.id_Horario=await result.id_Horario;
        console.log("ID HORARio", this.id_Horario)

        this.http.post(`${baseURL}medicacion/create/${this.id_Horario}` , JSON.stringify(medicina),options).subscribe((json: any)=> 
          {
              return(json)
          },(error)=>{
            
            return(error)
          });
        
      });
    }



    async mostrarMedicamentos() {
      let res:any;
      this.storage.get('infoCuenta').then(async (result) => {
        this.id_Horario=await result.id_Horario;
        
        //let res=await this.http.get<ListaMedicamentos>(`${baseURL}medicacion/all/${this.id_Horario}`);
          res=await(await fetch(`${baseURL}medicacion/all/${this.id_Horario}`, {
          method: "GET"
        })).json();
        
        this.storage.set('listaMedicacion',res['data']);

        
        
        
      });
      

    }

    async addNewItemAgenda(itemAgenda:any){

      console.log(itemAgenda)

      this.storage.get('infoCuenta').then(async (result) => {
        this.id_Agenda=await result.id_Agenda;
        console.log("id_Agenda:", this.id_Agenda)

        this.http.post(`${baseURL}agenda-item/create/${this.id_Agenda}` , JSON.stringify(itemAgenda),options).subscribe((json: any)=> 
          {
              return(json)
          },(error)=>{
            
            return(error)
          });
        
      });

    }

    async mostrarItemsAgenda(){

      let todayDate= new Date;
      let today=todayDate.toDateString();

      let res:any;
      this.storage.get('infoCuenta').then(async (result) => {
        this.id_Agenda=await result.id_Agenda;
        
          res=await(await fetch(`${baseURL}agenda-item/all/${this.id_Agenda}/${today}`, {
          method: "GET"
        })).json();
        
        this.storage.set('listaAgenda',res['data']);

      });

    }


    async mostrarFotos(){
      let res: any;
      this.storage.get('infoCuenta').then(async (result) => {
        this.id_Album=await result.id_Album;
        
          res=await(await fetch(`${baseURL}fotos/all/${this.id_Album}`, {
          method: "GET"
        })).json();
        
        this.storage.set('listaFotos',res['data']);

      });


    }

    async addNewFoto(newFoto:any){

      this.storage.get('infoCuenta').then(async (result) => {
        this.id_Album=await result.id_Album;
        

        this.http.post(`${baseURL}fotos/create/${this.id_Album}` , JSON.stringify(newFoto),options).subscribe((json: any)=> 
          {
              return(json)
          },(error)=>{
            
            return(error)
          });
        
      });

    }

    async addNewTel(newTel:any){

      let contacto:"http://placeimg.com/640/480/people";

      //newTel.numero_tel_imagen=contacto

      this.storage.get('infoCuenta').then(async (result) => {
        this.id_Listin=await result.id_Listin;
        

        this.http.post(`${baseURL}tel/create/${this.id_Listin}` , JSON.stringify(newTel),options).subscribe((json: any)=> 
          {
              return(json)
          },(error)=>{
            
            return(error)
          });
        
      });

    }


    async mostrarTelefonosListin(){

      let res: any;
      this.storage.get('infoCuenta').then(async (result) => {
        this.id_Listin=await result.id_Listin;
        
          res=await(await fetch(`${baseURL}tel/all/${this.id_Listin}`, {
          method: "GET"
        })).json();
        
        this.storage.set('listaTelefonos',res['data']);

      });

    }
}

