import { TestBed } from '@angular/core/testing';

import { DataBaseFireBaseService } from './data-base-fire-base.service';

describe('DataBaseFireBaseService', () => {
  let service: DataBaseFireBaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataBaseFireBaseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
