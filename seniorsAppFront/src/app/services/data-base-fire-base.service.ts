import { Injectable } from '@angular/core';
//import { firebaseConfig } from '../../environments/environment';


import { AngularFireModule } from '@angular/fire';
import { AngularFirestore } from '@angular/fire/firestore';
import firebase from 'firebase/app';


@Injectable({
  providedIn: 'root'
})
export class DataBaseFireBaseService {

  storage= this.dataBase.firestore.app.storage().ref()
  



  //fotoListAlbum: AngularFireList<any>;
  //fotoAlbum: AngularFireObject<any>;

  //fotoListListin: AngularFireList<any>;
  //fotoListin: AngularFireObject<any>;

  constructor(
    private db: AngularFireModule,
    public dataBase: AngularFirestore,
    
  ) {
    

    //this.dataBase.firestore
   }

  createDoc(data:any, path: string, id: string){
    const coleccion= this.dataBase.collection(path);
    return coleccion.doc(id).set(data);
  }

  getDoc(path:string, id:string){
    const coleccion= this.dataBase.collection(path);
    return coleccion.doc(id).valueChanges();

  }

  deleteDoc(path:string ,id:string){
    const coleccion= this.dataBase.collection(path);
    return coleccion.doc(id).delete();
  }

  updateDoc(data: any,path:string ,id:string){
    const coleccion= this.dataBase.collection(path);
    return coleccion.doc(id).update(data);
  }
  uploadPhoto = async (foto, path) => {

    let file = foto;
    let metadata = {
      contentType: "image/jpeg",
    };
    let uploadTask = await this.storage
      .child(path + file.name)
      .put(file, metadata);
    let url = await uploadTask.ref.getDownloadURL();
    return url;
   };
   



  /*uploadImage(file:any, path:string, nombre:string):Promise<string>{

    return new Promise( resolve => {

      const filePath = path + '/' + nombre;
      const ref = this.storage.ref(filePath);
      const task = ref.put(file);
      resolve('URL de la foto')

    });
      
  }*/


  encodeImageUri(imageUri, callback) {
    var c = document.createElement('canvas');
    var ctx = c.getContext("2d");
    var img = new Image();
    img.onload = function () {
      var aux:any = this;
      c.width = aux.width;
      c.height = aux.height;
      ctx.drawImage(img, 0, 0);
      var dataURL = c.toDataURL("image/jpeg");
      callback(dataURL);
    };
    img.src = imageUri;
  }

}
