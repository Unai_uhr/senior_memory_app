import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage-angular';
import { Login, Register ,InfoCuenta, DatosFormulario } from '../interfaces/interfaces';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';

const baseURL="https://unai-hernandez-7e3.alwaysdata.net/memory_senior/api/";

const options = {
  headers: {'Content-Type':'application/json'}
}

@Injectable({
  providedIn: 'root'
})
export class LoginDataService {

  datosCuenta:DatosFormulario={
    id:0,
    nombreRes:"",
    cuenta_email:"",
    nombre:"",
    apellido:"",
    nacimiento:"",
    genero:"",
    email:"",
    
  }

  constructor(
    private storage:Storage,
    private http: HttpClient,
    private navController:NavController
    ) { 

      this.storage.create();
    }


    login( strCuenta_email:string, strPassword:string){

          const datos = {
            cuenta_email:strCuenta_email,
            password : strPassword
          };
          this.http.post(`${baseURL}login` , JSON.stringify(datos),options).subscribe(async (json: Login)=> 
          {
              console.log(json['success']['id'])

              let idHorario=await (await fetch(`${baseURL}horario/getone/${json['success']['id']}`, {
                method: "GET"
              })).json();
              let idAlbum=await (await fetch(`${baseURL}album-fotos/getone/${json['success']['id']}`, {
                method: "GET"
              })).json();
              let idListin=await (await fetch(`${baseURL}listin-tel/getone/${json['success']['id']}`, {
                method: "GET"
              })).json();
              let idAgenda=await (await fetch(`${baseURL}agenda/getone/${json['success']['id']}`, {
                method: "GET"
              })).json();
              let usuario=await (await fetch(`${baseURL}usuarios/getone/${json['success']['id']}`, {
                method: "GET"
              })).json();
              let responsable=await (await fetch(`${baseURL}responsables/getone/${json['success']['id']}`, {
                method: "GET"
              })).json();
              //comprovamos si hay control de hoy
              let todayDate= new Date;
              let today=todayDate.toDateString();

              let controlHoyHay = await (await fetch(`${baseURL}control-medicacion/get-one/${idHorario['data'].id}/${today}`,{
                method:"GET"
              })).json();
              let controlHoy;
              let id_ControlHoyId:number;
              
              //SI no existe el de hoy se crea y se rellena en false
              if("error" in controlHoyHay){
                console.log("CONTROL ERROR", "error" in controlHoyHay);
                controlHoy = await (await fetch(`${baseURL}control-medicacion/create/${idHorario['data'].id}/${today}`,{
                  method:"POST"
                })).json();
                console.log("CONTROL", controlHoy);
                id_ControlHoyId=controlHoy.id;
                //Llenamos el control en false
                
                fetch(`${baseURL}items-control-medicacion/insert-all/${id_ControlHoyId}/${idHorario['data'].id}`,{
                  method:"POST"
                })

              //Si existe el de hoy se busca el id sin mas
              }else if('status' in controlHoyHay){
                console.log("CONTROL OK", "status" in controlHoyHay);
                id_ControlHoyId=controlHoyHay['data'].id;
              }
              
              


              

              let infoCuenta={
                idCuenta:json['success']['id'],
                usuario_nombre:usuario['data'].usuario_nombre,
                usuario_apellidos:usuario['data'].usuario_apellidos,
                usuario_fecha_nacimiento: usuario['data'].usuario_fecha_nacimiento,
                usuario_email:usuario['data'].usuario_email,
                cuenta_email:datos.cuenta_email,
                nombreRes:responsable['data'].responsable_nombre,
                id_Horario:idHorario['data'].id,
                id_Album:idAlbum['data'].id,
                id_Listin:idListin['data'].id,
                id_Agenda:idAgenda['data'].id,
                id_Control:id_ControlHoyId,
              }

              this.storage.set('infoCuenta',infoCuenta);
              this.navController.navigateForward('home');

          },(error)=>{
            return "error";
            console.log(error['error'])
          });
    }

    async createAcount(strCuenta_email:string, strPassword:string ,responsable_nombre:string){

      

      let rest= await (await fetch(`${baseURL}register`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json"
        },
        body: `{\"cuenta_email\":\"${strCuenta_email}\",\"password\":\"${strPassword}\"} `
      })).json();

      console.log(rest['success'].id);

     // this.datosCuenta.id=2;

      console.log(this.datosCuenta)
      //Guardamos en Storage los datos de la cuenta para sever que esta registrado y logado como usuario nuevo

      let idHorario=await (await fetch(`${baseURL}horario/create/${rest['success'].id}`, {
        method: "POST"
      })).json();
      let idAlbum=await (await fetch(`${baseURL}album-fotos/create/${rest['success'].id}`, {
        method: "POST"
      })).json();
      let idListin=await (await fetch(`${baseURL}listin-tel/create/${rest['success'].id}`, {
        method: "POST"
      })).json();
      let idAgenda=await (await fetch(`${baseURL}agenda/create/${rest['success'].id}`, {
        method: "POST"
      })).json();

      console.log(idHorario,idAlbum,idListin,idAgenda)

      let infoCuenta={
        idCuenta:rest['success'].id,
        cuenta_email: rest['success'].cuenta_email,
        nombreRes:responsable_nombre,
        id_Horario:idHorario.id,
        id_Album:idAlbum.id,
        id_Listin:idListin.id,
        id_Agenda:idAgenda.id
      }

      /*this.datosCuenta.id=rest['success'].id;
      this.datosCuenta.cuenta_email= rest['success'].cuenta_email;
      this.datosCuenta.nombreRes=responsable_nombre;*/

        
      
      this.storage.set('infoCuenta',infoCuenta);
      
      return rest.success;

        
      }
}
