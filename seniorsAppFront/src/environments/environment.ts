// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBKA-VEvOui51xCg3ekPEkLR7MG7GHrphM",
    authDomain: "seniormemorytrainerapp.firebaseapp.com",
    projectId: "seniormemorytrainerapp",
    storageBucket: "seniormemorytrainerapp.appspot.com",
    messagingSenderId: "500393367316",
    appId: "1:500393367316:web:3733df29e0f11da1641401"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
